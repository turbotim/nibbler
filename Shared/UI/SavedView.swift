//
//  SavedView.swift
//  Nibbler
//
//  Created by Tim Harrison on 26/07/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

class SavedView: UIView {

    private let backgroundView = UIView(frame: .zero)
    private let messageLabel = UILabel(frame: .zero)
    private let messageText: String
    
    init(with text: String) {
        
        self.messageText = text
        super.init(frame: .zero)
        addSubview(backgroundView)
        addSubview(messageLabel)
        configureViews()
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundView.layer.masksToBounds = true
        backgroundView.layer.cornerRadius = 15.0
        backgroundView.backgroundColor = .black
        backgroundView.alpha = 0.7
        messageLabel.numberOfLines = 0
        messageLabel.font = UIFont(descriptor: .preferredFontDescriptor(withTextStyle: .body), size: 40)
        messageLabel.textColor = .white
        messageLabel.text = self.messageText
        messageLabel.textAlignment = .center
    }
    
    func installConstraints() {
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        let views = ["backgroundView": backgroundView, "messageLabel": messageLabel]
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[backgroundView]|", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[backgroundView]|", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[messageLabel]|", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[messageLabel]|", options: [], metrics: nil, views: views))
    }
}
