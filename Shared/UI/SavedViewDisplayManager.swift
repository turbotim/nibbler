//
//  SavedViewDisplayManager.swift
//  Nibbler
//
//  Created by Tim Harrison on 19/11/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import UIKit

class SavedViewDisplayManager {
    
    private var savedView: SavedView?
    
    func hideMessage(completion: @escaping () -> Void) {
        
        guard let savedView = self.savedView else {
            
            completion()
            return
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            
            savedView.alpha = 0
            
        }) { _ in
            
            savedView.removeFromSuperview()
            self.savedView = nil
            completion()
        }
    }
    
    func show(message: String, in view: UIView, completion: @escaping () -> Void) {
        
        self.hideMessage { [weak self] in
            
            guard let strongSelf = self else { return }
            
            let savedView = SavedView(with: message)
            view.addSubview(savedView)
            savedView.translatesAutoresizingMaskIntoConstraints = false
            view.addConstraint(NSLayoutConstraint.init(item: savedView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0.0))
            view.addConstraint(NSLayoutConstraint.init(item: savedView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1.0, constant: 0.0))
            view.addConstraint(NSLayoutConstraint.init(item: savedView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 250.0))
            view.addConstraint(NSLayoutConstraint.init(item: savedView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 250.0))
            
            savedView.transform = CGAffineTransform.init(scaleX: 0.0, y: 0.0)
            strongSelf.savedView = savedView
            UIView.animate(withDuration: 0.5,
                           delay: 0.0,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 0.5,
                           options: [],
                           animations: {
                            
                            savedView.transform = CGAffineTransform.identity
                            
                           }, completion: { _ in
                            
                            completion()
                           })
        }
    }
}
