//
//  RecipeQueue.swift
//  Nibbler
//
//  Created by Tim Harrison on 18/11/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import Foundation

struct RecipeQueue {
    
    private let decoder = JSONDecoder()
    private let encoder = JSONEncoder()
    private let sharedDefaults = UserDefaults(suiteName: "group.nibbler.cookery")
    private let recipeQueueKey = "com.nibbler.recipeQueueKey"
    
    func store(recipe: WebRecipe) -> Bool {
        
        guard let sharedDefaults = self.sharedDefaults else { return false }
        
        var updatedQueue = [Data]()
        
        if let currentQueue = sharedDefaults.object(forKey: self.recipeQueueKey) as? [Data] {
            
            updatedQueue = currentQueue
        }
        
        guard let recipeData = try? self.encoder.encode(recipe) else {
            
            return false
        }
        updatedQueue.append(recipeData)
        sharedDefaults.setValue(updatedQueue, forKey: self.recipeQueueKey)
        return true
    }
    
    func clear() {
        
        guard let sharedDefaults = self.sharedDefaults else {
            
            return
        }
        
        sharedDefaults.removeObject(forKey: self.recipeQueueKey)
    }
    
    func recipesToStore() -> [WebRecipe] {
        
        guard let sharedDefaults = self.sharedDefaults,
              let currentQueue = sharedDefaults.object(forKey: self.recipeQueueKey) as? [Data] else {
            
            return []
        }
        
        
        let recipes = currentQueue.compactMap {
            
            return try? self.decoder.decode(WebRecipe.self, from: $0)
        }
        return recipes
    }
}
