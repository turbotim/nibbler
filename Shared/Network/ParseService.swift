//
//  ParseService.swift
//  Nibbler
//
//  Created by Tim Harrison on 15/11/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import Foundation

enum ParseServiceError: Error {
    
    case urlError
    case parseError
    case networkError
}

class ParseService {

    private let serverURLString = "https://us-central1-food-295320.cloudfunctions.net/parse_recipe"
    private let urlSession = URLSession.shared
    private let decoder = JSONDecoder()
    
    func recipe(for url: URL, completion: @escaping (Result<WebRecipe, Error>) -> Void) {
        
        guard var requestURLComponents = URLComponents(string: self.serverURLString) else {
            
            completion(.failure(ParseServiceError.urlError))
            return
        }

        requestURLComponents.queryItems = [
            URLQueryItem(name: "recipeURL", value: url.absoluteString)
        ]
        
        guard let requestURL = requestURLComponents.url else {
            
            completion(.failure(ParseServiceError.urlError))
            return
        }
        
        let dataTask = self.urlSession.dataTask(with: requestURL) { [weak self] data, urlResponse, error in
            
            DispatchQueue.main.async {
                
                self?.handleResponse(data: data, error: error, completion: completion)
            }
        }
        
        dataTask.resume()
     }
    
    private func handleResponse(data: Data?,
                                error: Error?,
                                completion: @escaping (Result<WebRecipe, Error>) -> Void) {
        
        if let data = data {
            
            if let webRecipe = self.parse(data: data) {
                
                completion(.success(webRecipe))
                
            } else {
                
                completion(.failure(ParseServiceError.parseError))
            }
            
        } else if let error = error {
            
            completion(.failure(error))
        } else {
            
            completion(.failure(ParseServiceError.networkError))
        }
    }
    
    private func parse(data: Data) -> WebRecipe? {

        do {
            
            let webRecipe = try self.decoder.decode(WebRecipe.self, from: data)
            return webRecipe
            
        } catch {
            
            print(error)
            return nil
        }
    }
}
