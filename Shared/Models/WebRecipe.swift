//
//  Recipe.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 10/02/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation

public struct RecipeSection: Codable {
    public let title: String?
    public let body: [String]
    public init(title: String?, body: [String]) {
        self.title = title
        self.body = body
    }
    var jsonDictionary: [String: Any] {
        return ["title": title ?? "", "body": body]
    }
}

public class WebRecipe: Codable {

    public let cookTime: TimeInterval?
    public let ingredients: [RecipeSection]
    public let method: [RecipeSection]
    public var otherText: [RecipeSection]
    public let title: String
    public let source: String
    public let sourceURL: URL
    public let imageURL: URL?
    
    public init(title:String, method:[RecipeSection], ingredients:[RecipeSection], otherText: [RecipeSection], cookTime:TimeInterval?, sourceURL:URL, source:String, imageURL:URL?) {
        
        self.cookTime = cookTime
        self.ingredients = ingredients
        self.method = method
        self.otherText = otherText
        self.title = title
        self.source = source
        self.sourceURL = sourceURL
        self.imageURL = imageURL
    }
}
