from recipe_scrapers import scrape_me
import json

def parse_recipe(request):

    recipe_url = ""
    if request.args and 'recipeURL' in request.args:
        recipe_url = request.args.get('recipeURL')
    else:
        return "No Recipe URL"

    if not isinstance(recipe_url, str):
        return "No Recipe URL"

    scraper = scrape_me(recipe_url, wild_mode=True)

    title = scraper.title()
    cookTime = scraper.total_time()
    author = scraper.host()
    image = scraper.image()
    ingredients = {
        "title": "",
        "body": scraper.ingredients()
    }

    method = {
        "title": "",
        "body": scraper.instructions().splitlines()
    }

    recipe = {
        "title": title,
        "cookTime": cookTime,
        "ingredients": [ingredients],
        "method": [method],
        "otherText": [],
        "source": author,
        "sourceURL": recipe_url,
        "imageURL": image,
    }
    return json.dumps(recipe)

 