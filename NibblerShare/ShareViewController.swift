//
//  ShareViewController.swift
//  NibblerShare
//
//  Created by Tim Harrison on 18/11/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import UIKit
import Social

class ShareViewController: UIViewController {

    private let recipeQueue = RecipeQueue()
    private let parseService = ParseService()
    private let savedViewDisplayManager = SavedViewDisplayManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.saveSharedRecipe()
    }

    func saveSharedRecipe() {
        
        guard let item = extensionContext?.inputItems.first as? NSExtensionItem,
              let itemProvider = item.attachments?.first,
              itemProvider.hasItemConformingToTypeIdentifier("public.url") else {
            
            self.extensionContext?.completeRequest(returningItems: [], completionHandler:nil)
            return
        }
        
        self.savedViewDisplayManager.show(message: "Saving...", in: self.view, completion: {})
        
        itemProvider.loadItem(forTypeIdentifier: "public.url", options: nil) { [weak self] (url, error) -> Void in
            
            self?.parseRecipe(url: url as? URL) { [weak self] in
                
                self?.extensionContext?.completeRequest(returningItems: [], completionHandler:nil)
            }
        }
    }
    
    private func parseRecipe(url: URL?, completion: @escaping () -> Void) {
        
        guard let shareURL = url else {
            
            self.savedViewDisplayManager.show(message: "Failed to save 🙁", in: self.view, completion: {
                
                completion()
            })
            return
        }

        self.parseService.recipe(for: shareURL) { result in
            
            switch result {
            
            case .failure:
                
                self.savedViewDisplayManager.show(message: "Failed to save 🙁", in: self.view, completion: {
                    
                    completion()
                })
            
            case .success(let recipe):
                
                let message = self.recipeQueue.store(recipe: recipe) ? "Saved 😊" : "Failed to save 🙁"
                self.savedViewDisplayManager.show(message: message, in: self.view, completion: {
                    
                    completion()
                })
            }
        }
    }
}
