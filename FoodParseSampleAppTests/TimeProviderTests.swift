//
//  TimeProviderTests.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 04/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import XCTest
@testable import FoodParse

class TimeProviderTests: XCTestCase {

    let timeProvider = TimeProvider()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func timeCheck(text:String, expectedMinutes:TimeInterval) {
        
        let minutes = timeProvider.time(fromText: text)
        XCTAssert(minutes == expectedMinutes, "should be \(String(describing: expectedMinutes)) minutes but was \(String(describing: minutes))")
    }
    
    func totalTimeCheck(prepTime:String?, cookTime:String?, expectedMinutes:TimeInterval?) {
    
        let minutes = timeProvider.totalTime(prepTimeText: prepTime, cookTimeText: cookTime)
        XCTAssert(minutes == expectedMinutes, "total time should be \(String(describing: expectedMinutes)) but was minutes")
    }

    func testNoNumberReturnsNil() {
    
        let minutes = timeProvider.time(fromText: "No numbers here")
        XCTAssertNil(minutes, "No numbers in the string should mean nil")
    }
    
    func testNoUnitsReturnsNil() {
        
        let minutes = timeProvider.time(fromText: "just 30 with no units")
        XCTAssertNil(minutes, "No units in the string should mean nil")
    }
    
    func testTimeLessThan30Mins() {
        timeCheck(text: "less than 30 mins", expectedMinutes: 30)
    }

    func testTime30to40Mins() {
        timeCheck(text: "30 to 40 mins", expectedMinutes: 40)
    }
    
    func testTime1Hour() {
        timeCheck(text: "1 hour", expectedMinutes: 60)
    }
    
    func testTime1To2Hours() {
        timeCheck(text: "1 to 2 hours", expectedMinutes: 120)
    }
    
    func testTime1Dash2Hours() {
        timeCheck(text: "1-2 hours", expectedMinutes: 120)
    }
    
    func testTimeCanHandleColons() {
        timeCheck(text: "  0:20 Cook", expectedMinutes: 20)
    }
    
    func testTimeCanHandleColonsAndHours() {
        timeCheck(text: "  3:20 Prep", expectedMinutes: 200)
    }
    
    func testCanStripOutNumbersFromButtedText() {
        timeCheck(text: "Cooks In45 minutes ", expectedMinutes: 45)
    }
    
    func testTotalTimeCanHandleMinutesAndHours() {
        totalTimeCheck(prepTime: "less than 1 hour", cookTime: "30 to 40 minutes", expectedMinutes: 100)
    }
    
    func testTotalTimeCanHandleANilPrep() {
        totalTimeCheck(prepTime: nil, cookTime: "about 2 hours", expectedMinutes: 120)
    }
    
    func testTotalTimeCanHandleANilCook() {
        totalTimeCheck(prepTime: "about 2 hours", cookTime: nil, expectedMinutes: 120)
    }
    
    func testTotalTimeReturnsNilIfPassedAllNils() {
        totalTimeCheck(prepTime: nil, cookTime: nil, expectedMinutes: nil)
    }
    
    func testReturnsNilWhen0MinsFound() {
        totalTimeCheck(prepTime: "0 hours", cookTime: "0 mins", expectedMinutes: nil)
    }
    
}
