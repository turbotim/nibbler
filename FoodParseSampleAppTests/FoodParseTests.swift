//
//  FoodParseTests.swift
//  FoodParseTests
//
//  Created by Tim Harrison on 05/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import XCTest
@testable import FoodParse

class FoodParseTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCanParseBBC() {
        let parserProvider = ParserProvider()
        let bbcURL = URL(string: "http://www.bbc.co.uk/food/whatever")!
        let parser = parserProvider.parser(forURL: bbcURL)!
        
        let bundle = Bundle(for:self.classForCoder)
        guard let location = bundle.path(forResource: "BBC-Food", ofType: "html") else {
            fatalError()
        }
        guard let html = try? String(contentsOfFile: location, encoding: String.Encoding.utf8),
            let webRecipe = parser.parse(html: html, source: bbcURL) else {
                
                XCTAssert(false,"could not parse bbc")
                return
        }
        
        XCTAssert(webRecipe.ingredients.count == 2, "ingredients should have 2 elements")
    }
    
    func testCanParseJamieOliverImage() {
        let parserProvider = ParserProvider()
        let bbcURL = URL(string: "http://www.jamieoliver.com/recipes")!
        let parser = parserProvider.parser(forURL: bbcURL)!
        
        let bundle = Bundle(for:self.classForCoder)
        guard let location = bundle.path(forResource: "Jamie-Oliver", ofType: "html") else {
            fatalError()
        }
        guard let html = try? String(contentsOfFile: location, encoding: String.Encoding.utf8),
            let webRecipe = parser.parse(html: html, source: bbcURL) else {
                
                XCTAssert(false,"could not parse Jamie")
                return
        }
        
        XCTAssert(webRecipe.imageURL?.absoluteString == "http://cdn.jamieoliver.com/recipe-database/430_575/49213398.jpg", "image url was not properly parsed")
    }
    
}
