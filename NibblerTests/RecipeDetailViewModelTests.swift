//
//  RecipeDetailViewModelTests.swift
//  NibblerTests
//
//  Created by Tim Harrison on 01/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import XCTest
@testable import Nibbler

class RecipeMock: NSObject, RecipeModel {
    var cookTime: Double = 0.0
    var imageURL: String?
    var ingredients: String?
    var method: String?
    var notes: String?
    var recipeJson: String?
    var sourceName: String?
    var sourceURL: String?
    var title: String?
    init(cookTime: Double, imageURL: String?, ingredients: String?, method: String?, notes: String?, recipeJson: String?, sourceName: String?, sourceURL: String?, title: String?) {
        self.cookTime = cookTime
        self.imageURL = imageURL
        self.ingredients = ingredients
        self.method = method
        self.notes = notes
        self.recipeJson = recipeJson
        self.sourceName = sourceName
        self.sourceURL = sourceURL
        self.title = title
    }
}

class RecipeModifiableMock: RecipeWritable {
    func store(webRecipe: WebRecipe) -> RecipeModel? {
        return nil
    }
    
    var wasSaved: Bool = false
    func save() {
        wasSaved = true
    }
    func savedRecipe(from webRecipe: WebRecipe) -> RecipeModel {
        return RecipeMock(cookTime: 0, imageURL: nil, ingredients: nil, method: nil, notes: nil, recipeJson: nil, sourceName: nil, sourceURL: nil, title: nil)
    }
    func delete(recipe: RecipeModel) {
        
    }
    func registerForNewRecipeUpdates(recipeAddtionCompletion: @escaping () -> ()) {
        
    }
}

class RecipeDetailViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRecipePropertiesAreCorrect() {
        let imageURL = "http://imageurl.com"
        let ingredients = "TEST INGREDIENTS"
        let method = "TEST METHOD"
        let notes = "TEST NOTES"
        let sourceURL = "http://sourceURL.com"
        let title = "TEST TITLE"
        let recipe = RecipeMock(cookTime: 0, imageURL: imageURL, ingredients: ingredients, method: method, notes: notes, recipeJson: nil, sourceName: nil, sourceURL: sourceURL, title: title)
        let recipeModifiable = RecipeModifiableMock()
        let recipeDetailViewModel = RecipeDetailViewModel(recipe: recipe, recipeSave: recipeModifiable)
        XCTAssertEqual(recipeDetailViewModel.imageURL?.absoluteString, imageURL)
        XCTAssertEqual(recipeDetailViewModel.ingredients, ingredients)
        XCTAssertEqual(recipeDetailViewModel.method, method)
        XCTAssertEqual(recipeDetailViewModel.notes, notes)
        XCTAssertEqual(recipeDetailViewModel.sourceURL?.absoluteString, sourceURL)
        XCTAssertEqual(recipeDetailViewModel.title, title)
    }
    
    func testNotesUpdateUpdatesModel() {
        let notes = "TEST NOTES"
        let recipe = RecipeMock(cookTime: 0, imageURL: nil, ingredients: nil, method: nil, notes: notes, recipeJson: nil, sourceName: nil, sourceURL: nil, title: nil)
        let recipeModifiable = RecipeModifiableMock()
        let recipeDetailViewModel = RecipeDetailViewModel(recipe: recipe, recipeSave: recipeModifiable)
        recipeDetailViewModel.updateNotes(notes: "UPDATED NOTES")
        XCTAssertTrue(recipeModifiable.wasSaved)
        XCTAssertEqual(recipe.notes, "UPDATED NOTES")
    }
    
    func testUpdatesArePassedToObservers() {
        let recipe = RecipeMock(cookTime: 0, imageURL: nil, ingredients: nil, method: nil, notes: nil, recipeJson: nil, sourceName: nil, sourceURL: nil, title: nil)
        let recipeModifiable = RecipeModifiableMock()
        let recipeDetailViewModel = RecipeDetailViewModel(recipe: recipe, recipeSave: recipeModifiable)
        let callBackExpectation = XCTestExpectation()
        recipeDetailViewModel.notesUpdated = {
            callBackExpectation.fulfill()
        }
        recipeDetailViewModel.updateNotes(notes: "UPDATED NOTES")
        wait(for: [callBackExpectation], timeout: 1.0)
    }
    
}




