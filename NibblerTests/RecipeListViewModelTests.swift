//
//  RecipeListViewModelTests.swift
//  NibblerTests
//
//  Created by Tim Harrison on 01/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import XCTest
@testable import Nibbler

class RecipeReadableModifiableMock: RecipeWritable, RecipeReadable {
    
    func store(webRecipe: WebRecipe) -> RecipeModel? {
        return nil
    }
    
    var wasDeleted: Bool = false
    var deletedRecipe: RecipeModel? = nil
    let recipe: RecipeModel = RecipeMock(cookTime: 0, imageURL: nil, ingredients: nil, method: nil, notes: nil, recipeJson: nil, sourceName: nil, sourceURL: nil, title: nil)
    
    func fetchAll(completion: @escaping ([RecipeModel]?) -> Void) {
        completion([recipe])
    }
    
    func save() {
        
    }
    func savedRecipe(from webRecipe: WebRecipe) -> RecipeModel {
        return recipe
    }
    func delete(recipe: RecipeModel) {
        deletedRecipe = recipe
    }
    func registerForNewRecipeUpdates(recipeAddtionCompletion: @escaping () -> ()) {
        
    }
}

class RecipeListViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDeletesRecipe() {
        let recipeAccessMock = RecipeReadableModifiableMock()
        let viewModel = RecipeListViewModel(recipeAccess: recipeAccessMock)
        viewModel.performFetch()
        XCTAssertEqual(viewModel.recipeCount(), 1)
        viewModel.delete(indexPath: IndexPath(item: 0, section: 0))
        guard let deletedRecipe = recipeAccessMock.deletedRecipe as? RecipeMock, let originalRecipe = recipeAccessMock.recipe as? RecipeMock else {
            XCTFail()
            return
        }
        XCTAssertEqual(originalRecipe, deletedRecipe)
    }
    
    func testFetchesRecipes() {
        let recipeAccessMock = RecipeReadableModifiableMock()
        let viewModel = RecipeListViewModel(recipeAccess: recipeAccessMock)
        viewModel.performFetch()
        guard let viewModelRecipe = viewModel.recipe(indexPath: IndexPath(item: 0, section: 0)) as? RecipeMock,
            let originalRecipe = recipeAccessMock.recipe as? RecipeMock else {
                XCTFail()
                return
        }
        XCTAssertEqual(originalRecipe, viewModelRecipe)
    }
    
}
