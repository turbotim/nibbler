//
//  RecipeListPresenter.swift
//  Nibbler
//
//  Created by Tim Harrison on 23/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

protocol RecipeListInteractorInput {
    var recipeSave: RecipeWritable {get}
    func performFetch()
    func importRecipes()
    func delete(indexPath: IndexPath)
    func recipeCellViewModel(indexPath: IndexPath) -> RecipeCellViewModel?
    func recipeCount() -> Int
    func recipe(indexPath: IndexPath) -> RecipeModel?
}

struct RecipeCellViewModel: Equatable {
    
    let title: String
    let sourceName: String
    let cookTime: String
    let defaultRecipeImage: UIImage
    let imageURL: URL?
}

class RecipeListInteractor: NSObject, RecipeListInteractorInput {

    private let recipeAccess: RecipeWritable & RecipeReadable
    weak var recipeListDisplay: RecipeListDisplay?
    var recipeSave: RecipeWritable {return recipeAccess}
    private let recipeImport: RecipeImport
    private var recipes = RecipeFilter()
    private var recipeCellViewModels: [RecipeCellViewModel] = [RecipeCellViewModel]() {
        didSet {
            if oldValue != self.recipeCellViewModels || self.recipeCellViewModels.isEmpty {
                
                self.recipeListDisplay?.recipesChanged()
            }
        }
    }
    var noResultsDueToFilter: Bool {
        return self.recipes.noResultsDueToFilters
    }
    
    init(recipeAccess: RecipeWritable & RecipeReadable) {
        self.recipeAccess = recipeAccess
        self.recipeImport = RecipeImport(recipeAccess: recipeAccess)
        super.init()
        self.recipeAccess.registerForNewRecipeUpdates { [weak self] in
            self?.performFetch()
        }
    }
    
    func delete(indexPath: IndexPath) {
        guard indexPath.item < recipes.filteredRecipes.count else {
            return
        }
        let recipeToDelete = recipes.filteredRecipes[indexPath.item]
        recipeAccess.delete(recipe: recipeToDelete)
        self.recipeListDisplay?.recipeDeleted(at: indexPath)
        recipes.remove(recipe: recipeToDelete)
        recipeCellViewModels.remove(at: indexPath.item)
    }
    
    func importRecipes() {
        
        self.recipeImport.importRecipes()
    }
    
    func performFetch() {
        recipeAccess.fetchAll { [weak self] (recipes)  in
            guard let recipes = recipes, let strongSelf = self else {
                return
            }
            strongSelf.recipes.configure(with: recipes)
            strongSelf.updateRecipeCellViewModels()
        }
    }
    
    func updateRecipeCellViewModels() {
        let recipes = self.recipes.filteredRecipes
        recipeCellViewModels = recipes.map { (recipe) -> RecipeCellViewModel in
            let cookTime: String
            if recipe.cookTime > 0 {
                cookTime = "\(Int(recipe.cookTime)) mins"
            } else {
                cookTime = ""
            }
            let recipeTitle = recipe.title ?? ""
            let sourceName = recipe.sourceName ?? ""
            let imageURL: URL?
            let defaultRecipeImage = #imageLiteral(resourceName: "DefaultRecipe")
            if let imageURLString = recipe.imageURL, let recipeImageURL = URL(string:URLFix.fix(url:imageURLString)) {
                imageURL = recipeImageURL
            } else {
                imageURL = nil
            }
            return RecipeCellViewModel(title: recipeTitle, sourceName: sourceName, cookTime: cookTime, defaultRecipeImage: defaultRecipeImage, imageURL: imageURL)
        }
    }
    
    func recipeCellViewModel(indexPath: IndexPath) -> RecipeCellViewModel? {
        guard indexPath.item < recipeCellViewModels.count else {
            return nil
        }
        return recipeCellViewModels[indexPath.item]
    }
    
    func recipeCount() -> Int {
        return recipeCellViewModels.count
    }
    
    func recipe(indexPath: IndexPath) -> RecipeModel? {
        guard indexPath.item < recipes.filteredRecipes.count else {
            return nil
        }
        return recipes.filteredRecipes[indexPath.item]
    }
    
    @objc private func recipeSaved() {
        performFetch()
    }
}

extension RecipeListInteractor: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {

        let filterText = searchController.searchBar.text ?? ""
        self.recipes.filter(with: filterText)
        self.updateRecipeCellViewModels()
    }
}
