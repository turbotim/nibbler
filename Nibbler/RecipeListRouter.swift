//
//  RecipeListRouter.swift
//  Nibbler
//
//  Created by Tim Harrison on 23/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class RecipeListRouter {

    private weak var presentingViewController: UIViewController?
    private var navigationController: UINavigationController? = nil
    private let transistionDelegate = ShowRecipeCustomTransitionDelegate()
    private let recipeAccess: RecipeAccess
    
    init(recipeAccess: RecipeAccess) {
        self.recipeAccess = recipeAccess
    }
    
    func attach(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
    
    func attach(viewController: UIViewController) {
        self.presentingViewController = viewController
    }
    
    func showWebViewController(url: URL) {
        let viewController = RecipeWebViewController(url: url, showsSaveButton: true, recipeAccess: recipeAccess)
        presentingViewController?.present(viewController, animated: true) {}
    }
    
    func showRecipeViewController(recipe: RecipeModel, recipeCell: RecipeCollectionViewCell) {
        let recipeDetailRouter = RecipeDetailRouter(recipeAccess: recipeAccess)
        let recipeDetailViewController = RecipeDetailViewController(recipeSave: recipeAccess, recipe: recipe, recipeDetailRouter: recipeDetailRouter)
        recipeDetailRouter.attach(viewController: recipeDetailViewController)
        self.navigationController?.delegate = transistionDelegate
        self.navigationController?.pushViewController(recipeDetailViewController, animated: true)
    }
}
