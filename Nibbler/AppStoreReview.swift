//
//  AppStoreReview.swift
//  Brewing
//
//  Created by Tim Harrison on 25/10/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import UIKit
import StoreKit

struct AppStoreReview {
    
    private let minimunSavedRecipes = 3
    private let hasAskedForReviewKey = "com.nibbler.hasReviewed"
    
    func requestReviewIfAppropriate(for recipeCount: Int) {
        let userDefaults = UserDefaults.standard
        
        let hasAskedForReview = userDefaults.bool(forKey: self.hasAskedForReviewKey)
        
        guard recipeCount >= self.minimunSavedRecipes,
            !hasAskedForReview else {
                
            return
        }
        
        userDefaults.set(true, forKey: self.hasAskedForReviewKey)
        SKStoreReviewController.requestReview()
    }
}
