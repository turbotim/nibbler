//
//  RecipeCollectionViewCell.swift
//  Nibbler
//
//  Created by Tim Harrison on 03/06/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

class RecipeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeTitleLabel: UILabel!
    @IBOutlet weak var recipeSourceLabel: UILabel!
    @IBOutlet weak var recipeCookTimeLabel: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    
    lazy var imageLoading = ImageLoading(imageView: self.recipeImageView)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundCardView.layer.cornerRadius = 8.0
        backgroundCardView.layer.masksToBounds = true
        backgroundCardView.backgroundColor = Theme.contentBackground
        recipeImageView.layer.cornerRadius = 8.0
        recipeImageView.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        recipeImageView.image = nil
        recipeSourceLabel.text = nil
        recipeTitleLabel.text = nil
        recipeCookTimeLabel.text = nil
    }

}
