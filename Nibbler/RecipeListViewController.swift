//
//  MenuViewController.swift
//  Nibbler
//
//  Created by Tim Harrison on 01/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit
import CoreData

protocol RecipeListTransitionDataProvider: class {
    var transitionIndexPath: IndexPath? { get }
    var collectionView: UICollectionView { get }
    var view: UIView! { get }
}

protocol RecipeListDisplay: class {
    
    func recipeDeleted(at indexPath: IndexPath)
    func recipesChanged()
}

class RecipeListViewController: UIViewController, RecipeListTransitionDataProvider {
    
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private let interactor: RecipeListInteractor
    private let recipeListRouter: RecipeListRouter
    private let appStoreReview = AppStoreReview()
    private var searchController = UISearchController()
    private let emptyLibraryView = EmptyLibraryView(frame: .zero)
    
    public var transitionIndexPath: IndexPath? = nil
    private var deleteController: DeleteController? = nil
    private let upgrade = Upgrade()
    
    required init?(coder aDecoder: NSCoder) {

        let recipeAccess = RecipeAccess.shared
        self.interactor = RecipeListInteractor(recipeAccess: recipeAccess)
        self.recipeListRouter = RecipeListRouter(recipeAccess: recipeAccess)
        super.init(coder: aDecoder)
        self.interactor.recipeListDisplay = self
        self.navigationItem.searchController = self.searchController
        self.searchController.searchResultsUpdater = self.interactor
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.searchTextField.placeholder = "Search your saved recipes"
        self.configureRouter()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.extendedLayoutIncludesOpaqueBars = true
        self.setUpView()
        deleteController = DeleteController(collectionView: collectionView)
        deleteController!.delegate = self
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        self.configureViewModel()
        NotificationCenter.default.addObserver(forName: AppDelegate.applicationDidBecomeActiveNotificationName,
                                               object: nil,
                                               queue: nil) { [weak self] _ in
            self?.interactor.importRecipes()
        }
        DispatchQueue.main.async { self.upgrade.performMigrationIfNeeded() }
    }
    
    private func configureRouter() {
        
        recipeListRouter.attach(viewController: self)
        recipeListRouter.attach(navigationController: navigationController)
    }
    
    private func configureViewModel() {
        
        interactor.performFetch()
    }
    
    private func setUpView() {
        
        title = "Recipes"
        self.view.addSubview(self.collectionView)
        self.view.pin(innerView: self.collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "RecipeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RecipeCollectionViewCell")
        collectionView.backgroundColor = Theme.background
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width, height: 121.0)
        
        self.view.addSubview(self.emptyLibraryView)
        self.view.pin(innerView: self.emptyLibraryView)
        self.emptyLibraryView.isHidden = true
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                                 target: self,
                                                                 action: #selector(addButtonTapped))
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: size.width, height: 121.0)
    }
    
    @objc private func addButtonTapped() {
        let google = URL(string: "https://www.google.com/")!
        self.recipeListRouter.showWebViewController(url: google)
    }
}

extension RecipeListViewController: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let cell = collectionView.cellForItem(at: indexPath) as! RecipeCollectionViewCell
        guard let recipe = interactor.recipe(indexPath: indexPath) else {
            return
        }
        self.transitionIndexPath = self.collectionView.indexPath(for: cell)
        recipeListRouter.showRecipeViewController(recipe: recipe, recipeCell: cell)
    }
}

extension RecipeListViewController: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interactor.recipeCount()
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecipeCollectionViewCell", for: indexPath) as! RecipeCollectionViewCell
        guard let recipeCellViewModel = interactor.recipeCellViewModel(indexPath: indexPath) else {
            return cell
        }
        cell.recipeTitleLabel.text = recipeCellViewModel.title
        cell.recipeSourceLabel.text = recipeCellViewModel.sourceName
        cell.recipeCookTimeLabel.text = recipeCellViewModel.cookTime
        cell.imageLoading.loadImage(imageURL: recipeCellViewModel.imageURL,
                                    placeholderImage: recipeCellViewModel.defaultRecipeImage)
        return cell
    }
}

extension RecipeListViewController: RecipeListDisplay {
    
    func recipeDeleted(at indexPath: IndexPath) {
    
        DispatchQueue.main.async {
            
            self.collectionView.deleteItems(at: [indexPath])
        }
    }
    
    func recipesChanged() {
        
        DispatchQueue.main.async {
            
            self.collectionView.reloadData()
            let recipeCount = self.interactor.recipeCount()
            self.emptyLibraryView.isHidden = (recipeCount > 0)
            let emptyState: EmptyLibraryView.EmptyState = self.interactor.noResultsDueToFilter ? .filteredReciped : .noRecipes
            self.emptyLibraryView.configure(for: emptyState)
            self.appStoreReview.requestReviewIfAppropriate(for: recipeCount)
        }
    }
}

extension RecipeListViewController: DeleteControllerDelegate {
    func deleteTapped(indexPath: IndexPath) {
        interactor.delete(indexPath: indexPath)
    }
}
