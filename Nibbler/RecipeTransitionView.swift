//
//  RecipeTransitionView2.swift
//  Nibbler
//
//  Created by Tim Harrison on 01/06/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

class RecipeTransitionView: UIView {

    let backgroundView = UIView(frame: CGRect.zero)
    let imageView = UIImageView(frame: CGRect.zero)
    let detailView = RecipeDetailContainerView(frame: .zero)
    
    init() {
        super.init(frame: CGRect.zero)
        backgroundView.backgroundColor = .systemBackground
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 8.0
        imageView.contentMode = .scaleAspectFill
        addSubview(backgroundView)
        addSubview(imageView)
        backgroundView.addSubview(detailView)
    }
    
    override init(frame: CGRect) {
       fatalError("init(frame:) has not been implemented")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
