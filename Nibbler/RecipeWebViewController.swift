//
//  RecipeWebViewController.swift
//  Nibbler
//
//  Created by Tim Harrison on 01/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit
import WebKit

class RecipeWebViewController: UIViewController {
    
    @IBOutlet weak var addressBar: UITextField!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingBarTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var forwardButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var bottomToolbar: UIToolbar!
    @IBOutlet weak var fakeNav: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var fakeNavHeightConstraint: NSLayoutConstraint!
    
    private let webView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
    private let showsSaveButton: Bool
    private var initialURL: URL
    private var htmlToLoad: String?
    private var loadedURL: URL?
    private var loading = false
    private let recipeProvider = RecipeProvider()
    private let recipeAccess: RecipeWritable
    private let parseService = ParseService()
    private let savedViewDisplayManager = SavedViewDisplayManager()
    
    init(url: URL, showsSaveButton: Bool, recipeAccess: RecipeWritable) {
        self.initialURL = url
        self.showsSaveButton = showsSaveButton
        self.recipeAccess = recipeAccess
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extendedLayoutIncludesOpaqueBars = true
        self.loadingBarTrailingConstraint.constant = UIScreen.main.bounds.width
        self.saveButton.isEnabled = false
        configureWebView()
        loadURL(url: initialURL)
        configureNav()
        addressBar.delegate = self
        addressBar.keyboardType = .webSearch
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        loading = false
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        dismiss(animated: true) {}
    }
    
    @IBAction func backTapped(_ sender: Any) {
        webView.goBack()
    }
    
    @IBAction func forwardTapped(_ sender: Any) {
        webView.goForward()
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        guard let url = webView.url else {
            return
        }
        saveButton.isEnabled = false
        self.fetchWebRecipe(from: url)
    }
    
    private func fetchWebRecipe(from url: URL) {
        
        self.savedViewDisplayManager.show(message: "Saving...", in: self.view, completion: {})
        self.parseService.recipe(for: url) { [weak self] result in
            
            switch result {
                
            case .failure:
                self?.displayFailure()
            case .success(let webRecipe):
                self?.storeRecipe(webRecipe: webRecipe)
            }
        }
    }
    
    private func loadURL(url:URL) {
        addressBar.text = url.absoluteString
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
    private func configureNav() {
        guard #available(iOS 11.0, *) else {
            return
        }
        view.removeConstraint(topConstraint)
        fakeNavHeightConstraint.constant = 44
        let fakeTop = UIView(frame: .zero)
        fakeTop.translatesAutoresizingMaskIntoConstraints = false
        fakeTop.backgroundColor = fakeNav.backgroundColor
        view.addSubview(fakeTop)
        NSLayoutConstraint.activate([
            fakeNav.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            fakeTop.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            fakeTop.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            fakeTop.topAnchor.constraint(equalTo: view.topAnchor),
            fakeTop.bottomAnchor.constraint(equalTo: fakeNav.topAnchor)
            ])
    }
    
    private func configureWebView() {
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: webView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: webView.trailingAnchor),
            bottomToolbar.topAnchor.constraint(equalTo: webView.bottomAnchor),
            fakeNav.bottomAnchor.constraint(equalTo: webView.topAnchor)
            ])
        webView.navigationDelegate = self
    }
    
    private func storeRecipe(webRecipe: WebRecipe) {
        
        _ = self.recipeAccess.store(webRecipe: webRecipe)
        self.savedViewDisplayManager.show(message: "Saved 😀", in: self.view) { [weak self] in
            self?.saveButton.isEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self?.dismiss(animated: true)
            }
        }
    }
    
    private func displayFailure() {
        
        self.savedViewDisplayManager.show(message: "Failed to Save 🙁", in: self.view) { [weak self] in
            
            self?.saveButton.isEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                
                self?.savedViewDisplayManager.hideMessage { }
            }
        }
    }
    
    private func updateLoadingState() {
        guard loading else {
            return
        }
        
        var loadingChange: CGFloat = 8.0
        
        if loadingBarTrailingConstraint.constant < 10 {
            loadingChange = 0.0
        } else if loadingBarTrailingConstraint.constant < 50 {
            loadingChange = 0.1
        } else if loadingBarTrailingConstraint.constant < 100 {
            loadingChange = 0.5
        } else if loadingBarTrailingConstraint.constant < 250 {
            loadingChange = 3.0
        }
        
        updateUIState()
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
            self.loadingBarTrailingConstraint.constant = self.loadingBarTrailingConstraint.constant - loadingChange
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }) { (complete) in
            DispatchQueue.main.async {
                self.updateLoadingState()
            }
        }
    }
    
    private func animateLoadingComplete() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            self.loadingBarTrailingConstraint.constant = 0
        }) { (complete) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                let width = self.view.bounds.width
                self.loadingBarTrailingConstraint.constant = width
                self.updateUIState()
            })
        }
    }
    
    private func updateUIState() {
        if let address = webView.url?.absoluteString, address != "" {
            addressBar.text = address
            let showsSaveButton = self.showsSaveButton
            self.saveButton.isEnabled = showsSaveButton
        }
        backButton.isEnabled = webView.canGoBack
        forwardButton.isEnabled = webView.canGoForward
    }
}

extension RecipeWebViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, let url = URL(string: text) {
            loadURL(url: url)
        }
        textField.resignFirstResponder()
        return true
    }
}

extension RecipeWebViewController: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        updateUIState()
        if loading || loadedURL == webView.url {
            return
        }
        loading = true
        let width = view.bounds.width
        loadingBarTrailingConstraint.constant = width
        updateLoadingState()
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if !loading {
            return
        }
        loading = false
        loadedURL = webView.url
        animateLoadingComplete()
    }
}
