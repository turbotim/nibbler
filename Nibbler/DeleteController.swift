//
//  DeleteHelper.swift
//  Nibbler
//
//  Created by Tim Harrison on 05/07/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

enum DeleteState {
    case none
    case scrolling
    case sliding
    case deleting
}

protocol DeleteControllerDelegate: class {
    func deleteTapped(indexPath:IndexPath)
}

class DeleteController: NSObject, DeleteRecipeDelegate, UIGestureRecognizerDelegate {
    
    var deleteState: DeleteState = .none
    let collectionView: UICollectionView
    var previousGesturePoint: CGPoint? = nil
    let deleteView = DeleteRecipeView(frame: CGRect(x: UIScreen.main.bounds.width, y: 0, width: 0, height: 121))
    var initialCellOriginX: CGFloat? = nil
    var deletingCellIndexPath: IndexPath? = nil
    var gestureRecognizer: UIPanGestureRecognizer! = nil
    weak var delegate: DeleteControllerDelegate? = nil
    
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        super.init()
        deleteView.delegate = self
        gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panRecognized))
        gestureRecognizer.delegate = self
        self.collectionView.addGestureRecognizer(gestureRecognizer)
        self.collectionView.addSubview(deleteView)
    }
    
    @objc func panRecognized(recognizer: UIPanGestureRecognizer) {
    
        if deleteState == .scrolling {
            return
        }
        
        let velocity = recognizer.velocity(in: collectionView)
        if deleteState == .none && abs(velocity.y) > abs(velocity.x) {
            return
        }
        
        let point = recognizer.location(in: self.collectionView)
        
        slideCell(point: point, recognizer: recognizer)
    }
    
    func slideCell(point: CGPoint, recognizer: UIPanGestureRecognizer) {
        
        switch recognizer.state {
        case .began:
            
            guard let swipeCellIndexPath = collectionView.indexPathForItem(at: point) else {
                return
            }
            
            if deleteState == .deleting && deletingCellIndexPath != swipeCellIndexPath {
                endDeleting()
            }
            
            if !(deleteState == .deleting && deletingCellIndexPath == swipeCellIndexPath) {
                guard let cell = self.collectionView.cellForItem(at: swipeCellIndexPath) as? RecipeCollectionViewCell else {
                    return
                }
                initialCellOriginX = cell.frame.origin.x
            }
            
            deletingCellIndexPath = swipeCellIndexPath
            previousGesturePoint = point
            startSliding()
            
        case .changed:
            
            guard let deletingCellIndexPath = deletingCellIndexPath else {
                return
            }
            
            guard let cell = self.collectionView.cellForItem(at: deletingCellIndexPath) as? RecipeCollectionViewCell else {
                return
            }
            
            let change = previousGesturePoint!.x - point.x
            var cellFrame = cell.frame
            cellFrame.origin.x -= change
            let cellOffset = initialCellOriginX! - cellFrame.origin.x
            if (cellOffset) > 0 {
                cell.frame = cellFrame
                var deleteFrame = deleteView.frame
                deleteFrame.origin.y = cellFrame.origin.y
                deleteFrame.origin.x = cellFrame.origin.x + cellFrame.size.width
                deleteFrame.size.width = cellOffset
                deleteView.frame = deleteFrame
            }
            previousGesturePoint = point
            
        case .ended, .cancelled:
            
            endSliding()
            
            guard let deletingCellIndexPath = deletingCellIndexPath else {
                return
            }
            
            guard let cell = self.collectionView.cellForItem(at: deletingCellIndexPath) as? RecipeCollectionViewCell else {
                return
            }
            
            guard let initialCellOriginX = initialCellOriginX else {
                return
            }
            
            deleteState = .deleting
            
            var cellFrame = cell.frame
            let cellOffset = initialCellOriginX - cellFrame.origin.x
            let cellDeleteOffset = cellFrame.size.width / 4
            
            if cellOffset < cellDeleteOffset {
                endDeleting()
                return
            }
            cellFrame.origin.x = initialCellOriginX - cellDeleteOffset
            
            let finalCellOffset = initialCellOriginX - cellFrame.origin.x
            var deleteFrame = deleteView.frame
            deleteFrame.origin.y = cellFrame.origin.y
            deleteFrame.origin.x = cellFrame.origin.x + cellFrame.size.width
            deleteFrame.size.width = finalCellOffset
            
            UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .curveLinear, animations: {
                
                cell.frame = cellFrame
                self.deleteView.frame = deleteFrame
                
            }, completion: { (completed) in
                
            })
            
            previousGesturePoint = nil
            
        case .failed, .possible:
            return
        @unknown default:
            fatalError()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if deleteState == .deleting {
            endDeleting()
        }
        deleteState = .scrolling
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            deleteState = .none
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        deleteState = .none
    }
    
    // MARK - Delete Cell
    
    func startSliding() {
        collectionView.isScrollEnabled = false;
        deleteState = .sliding
    }
    
    func endSliding() {
        collectionView.isScrollEnabled = true;
    }
    
    func endDeleting() {
        
        if let deletingCellIndexPath = deletingCellIndexPath,
            let initialCellOriginX = initialCellOriginX,
            let cell = collectionView.cellForItem(at: deletingCellIndexPath) {
            
            var cellFrame = cell.frame
            cellFrame.origin.x = initialCellOriginX
            
            let finalCellOffset = initialCellOriginX - cellFrame.origin.x
            var deleteFrame = deleteView.frame
            deleteFrame.origin.x = cellFrame.origin.x + cellFrame.size.width
            deleteFrame.size.width = finalCellOffset
            
            UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .curveLinear, animations: {
                
                cell.frame = cellFrame
                self.deleteView.frame = deleteFrame
                
            }, completion: { (completed) in
                
            })
        }
        
        self.initialCellOriginX = nil
        self.deletingCellIndexPath = nil
        deleteState = .none
    }
    
    func deleteTapped(recipeView: DeleteRecipeView) {
        
        guard let deletingCellIndexPath = deletingCellIndexPath else {
            return
        }
        endDeleting()
        if let delegate = delegate {
            delegate.deleteTapped(indexPath: deletingCellIndexPath)
        }
    }
}
