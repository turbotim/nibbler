//
//  WebRecipe+Text.swift
//  Nibbler
//
//  Created by Tim Harrison on 02/12/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

extension WebRecipe {
    
    public var ingredientsText: String {
        get {
            return text(sections: ingredients, isNumbered: false)
        }
    }
    
    public var methodText: String {
        get {
            
            let isNumbered = self.method.first?.body.count ?? 0 > 1
            return text(sections: method, isNumbered: isNumbered)
        }
    }
    
    func text(sections:[RecipeSection], isNumbered: Bool) -> String {
        
        var text = ""
        var sectionCount = 0
        for section in sections {
            
            let sectionTitle = section.title
            
            if sectionCount > 0 {
                text.append("\n")
            }
            
            if let title = sectionTitle, title.count > 0 {
                text.append("\(title)\n")
            }
            
            var number = 0
            for item in section.body {
                if isNumbered {
                    number = number + 1
                    text.append("\(number). \(item)\n\n")
                } else {
                    text.append("\(item)\n")
                }
            }
            
            sectionCount += 1
        }
        return text
    }
}
