//
//  ImageViewUtils.swift
//  Nibbler
//
//  Created by Tim Harrison on 31/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

class ImageLoading {
    
    private var currentImageURL: URL?
    private let imageView: UIImageView
    private let imageCache = ImageCache.shared
    
    init(imageView: UIImageView) {
        self.imageView = imageView
    }
    
    func loadImage(imageURL: URL?, placeholderImage: UIImage?) {
        
        self.imageView.image = placeholderImage
        
        guard let imageURL = imageURL else { return }
        
        self.currentImageURL = imageURL
        self.imageCache.loadImage(url: imageURL) { [weak self] cacheResult in
            
            guard let strongSelf = self,
                  strongSelf.currentImageURL == cacheResult.url,
                  let image = cacheResult.image else {
                
                return
            }
            
            strongSelf.imageView.image = image
        }
    }
}

class ImageCache {
    
    struct CacheResult {
        
        let image: UIImage?
        let url: URL
        let cacheHit: Bool
    }
    
    static let shared = ImageCache()
    
    private let imageCache = NSCache<NSString, UIImage>()
    private let urlSession = URLSession.shared
    
    func loadImage(url: URL, completion: @escaping (CacheResult) -> Void) {
        
        if let image = self.loadImageFromCache(url: url) {
            completion(CacheResult(image: image, url: url, cacheHit: true))
            return
        }
        
        if let image = self.loadImageFromDisk(url: url) {
            completion(CacheResult(image: image, url: url, cacheHit: true))
            return
        }
        
        self.loadImageFromNetwork(url: url) { (image) in
            
            DispatchQueue.main.async {
                
                completion(CacheResult(image: image, url: url, cacheHit: false))
            }
        }
    }
    
    private func loadImageFromNetwork(url: URL, completion: @escaping (UIImage?) -> Void) {
        
        let dataTask = self.urlSession.dataTask(with: url) { [weak self] (data, response, error) in
            
            guard let strongSelf = self,
                  let data = data,
                  let image = UIImage(data: data) else {
                
                completion(nil)
                return
            }
            
            strongSelf.storeInCache(image: image, url: url)
            strongSelf.storeOnDisk(image: image, url: url)
            completion(image)
        }
        
        dataTask.resume()
    }
    
    private func storeInCache(image: UIImage, url: URL) {
        
        self.imageCache.setObject(image, forKey: url.absoluteString as NSString)
    }
    
    private func loadImageFromCache(url: URL) -> UIImage? {
        
        return  imageCache.object(forKey: url.absoluteString as NSString)
    }
    
    private func storeOnDisk(image: UIImage, url: URL) {
        
        let filePath = self.diskCacheFile(from: url)
        try? image.pngData()?.write(to: URL(fileURLWithPath: filePath))
    }
    
    private func loadImageFromDisk(url: URL) -> UIImage? {
        
        let filePath = self.diskCacheFile(from: url)
        return UIImage(contentsOfFile: filePath)
    }
    
    private func diskCacheFile(from url: URL) -> String {
        
        let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as String
        
        let sanitisedURL = url.absoluteString.replacingOccurrences(of: "/", with: "").replacingOccurrences(of: ":", with: "")
        let filePath = "\(dir)/Image-\(sanitisedURL).png"
        
        return filePath
    }
}

