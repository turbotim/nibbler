//
//  Theme.swift
//  Nibbler
//
//  Created by Tim Harrison on 30/10/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: 1.0)
    }
}

enum Theme {
    
    static let plateBlue = UIColor { (traitCollection: UITraitCollection) -> UIColor in
        switch traitCollection.userInterfaceStyle {
        case
          .unspecified, .light:
            return UIColor(red: 34, green: 160, blue: 237)
        case .dark:
            return .systemBackground
        @unknown default:
            return UIColor(red: 34, green: 160, blue: 237)
        }
    }
    
    static let background = UIColor { (traitCollection: UITraitCollection) -> UIColor in
        switch traitCollection.userInterfaceStyle {
        case
          .unspecified, .light:
            return .systemGray5
        case .dark:
            return .systemBackground
        @unknown default:
            return .systemGray5
        }
    }
    
    static let contentBackground = UIColor { (traitCollection: UITraitCollection) -> UIColor in
        switch traitCollection.userInterfaceStyle {
        case
          .unspecified, .light:
            return .systemBackground
        case .dark:
            return .secondarySystemBackground
        @unknown default:
            return .systemBackground
        }
    }
    
    static func themeApp() {
        
        let navigationBarColor = self.plateBlue
        let titleTextColor = UIColor.white
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: titleTextColor]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: titleTextColor]
            navBarAppearance.backgroundColor = navigationBarColor
            UINavigationBar.appearance().standardAppearance = navBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
            UINavigationBar.appearance().compactAppearance = navBarAppearance
            
        } else {
            
            UINavigationBar.appearance().isTranslucent = false
            UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: titleTextColor]
            UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: titleTextColor]
            UINavigationBar.appearance().backgroundColor = navigationBarColor
            UINavigationBar.appearance().barStyle = .black
        }
        
        UISearchTextField.appearance().backgroundColor = .systemBackground
        UISearchTextField.appearance().tintColor = .black
        UINavigationBar.appearance().tintColor = titleTextColor
        UINavigationBar.appearance().barTintColor = navigationBarColor
    }
}
