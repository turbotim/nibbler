//
//  NotesViewController.swift
//  Nibbler
//
//  Created by Tim Harrison on 23/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

protocol NotesDelegate: class {
    func didFinishEditing(notes: String)
}

class NotesViewController: UIViewController {

    let textView = UITextView(frame: .zero)
    let originalNotes: String
    weak var delegate: NotesDelegate?
    
    init(notes: String) {
        originalNotes = notes
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view.addSubview(textView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigation()
        configureConstraints()
        configureTextView(notes: originalNotes)
        self.extendedLayoutIncludesOpaqueBars = true
    }
    
    private func configureNavigation() {
        title = "My Notes"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(closeTapped))
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    private func configureTextView(notes: String) {
        textView.text = notes
        textView.font = FontProvider.bodyFont
        textView.becomeFirstResponder()
    }
    
    private func configureConstraints() {
        textView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textView.topAnchor.constraint(equalTo: view.topAnchor),
            textView.rightAnchor.constraint(equalTo: view.rightAnchor),
            textView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            textView.leftAnchor.constraint(equalTo: view.leftAnchor)
            ])
    }
    
    @objc func closeTapped() {
        delegate?.didFinishEditing(notes: textView.text)
        textView.resignFirstResponder()
        dismiss(animated: true)
    }
}

