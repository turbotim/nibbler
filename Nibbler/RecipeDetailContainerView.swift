//
//  RecipeDetailContainerView.swift
//  Nibbler
//
//  Created by Tim Harrison on 28/11/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

protocol RecipeDetailDelegate: class {
    func originalButtonTapped()
    func notesButtonTapped()
}

class RecipeDetailContainerView: UIView {

    let ingredientsTitleLabel = UILabel(frame: .zero)
    let ingredientsBodyLabel = UILabel(frame: .zero)
    let ingredientsSeperatorView = UIView(frame: .zero)
    let methodSeperatorView = UIView(frame: .zero)
    let notesSeperatorView = UIView(frame: .zero)
    let methodTitleLabel = UILabel(frame: .zero)
    let methodBodyLabel = UILabel(frame: .zero)
    let notesTitleLabel = UILabel(frame: .zero)
    let notesBodyLabel = UILabel(frame: .zero)
    let seeOriginalButton = UIButton(type: .system)
    
    lazy var containerStackView: UIStackView = { UIStackView(arrangedSubviews: [ingredientsTitleLabel, ingredientsBodyLabel, ingredientsSeperatorView, methodTitleLabel, methodBodyLabel, methodSeperatorView, notesTitleLabel, notesBodyLabel, notesSeperatorView, seeOriginalButton]) }()
    weak var delegate: RecipeDetailDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    private func configureView() {
        addSubview(containerStackView)
        containerStackView.axis = .vertical
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            ingredientsSeperatorView.heightAnchor.constraint(equalToConstant: 10),
            methodSeperatorView.heightAnchor.constraint(equalToConstant: 2),
            notesSeperatorView.heightAnchor.constraint(equalToConstant: 20),
            containerStackView.topAnchor.constraint(equalTo: topAnchor),
            containerStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            containerStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            ])
        ingredientsTitleLabel.font = FontProvider.titleFont
        ingredientsTitleLabel.text = "Ingredients"
        
        ingredientsBodyLabel.font = FontProvider.bodyFont
        ingredientsBodyLabel.numberOfLines = 0
        ingredientsBodyLabel.font = FontProvider.bodyFont
        
        methodTitleLabel.font = FontProvider.titleFont
        methodTitleLabel.text = "Method"
        
        methodBodyLabel.font = FontProvider.bodyFont
        methodBodyLabel.numberOfLines = 0
        
        notesTitleLabel.font = FontProvider.titleFont
        notesTitleLabel.text = "My Notes"
        let notesTitleGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(notesButtonTapped))
        notesTitleLabel.addGestureRecognizer(notesTitleGestureRecogniser)
        notesTitleLabel.isUserInteractionEnabled = true
        
        notesBodyLabel.font = FontProvider.bodyFont
        notesBodyLabel.numberOfLines = 0
        let notesBodyGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(notesButtonTapped))
        notesBodyLabel.addGestureRecognizer(notesBodyGestureRecogniser)
        notesBodyLabel.isUserInteractionEnabled = true
        
        seeOriginalButton.setTitle("See Original", for: .normal)
        seeOriginalButton.addTarget(self, action: #selector(originalButtonTapped), for: .touchUpInside)
    }
    
    @objc private func originalButtonTapped() {
        delegate?.originalButtonTapped()
    }
    
    @objc private func notesButtonTapped() {
        delegate?.notesButtonTapped()
    }
}
