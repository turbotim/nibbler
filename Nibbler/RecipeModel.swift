//
//  RecipeModel.swift
//  Nibbler
//
//  Created by Tim Harrison on 01/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

protocol RecipeModel {
    var cookTime: Double {get set}
    var imageURL: String?  {get set}
    var ingredients: String? {get set}
    var method: String? {get set}
    var notes: String? {get set}
    var recipeJson: String? {get set}
    var sourceName: String? {get set}
    var sourceURL: String? {get set}
    var title: String? {get set}
}

extension Recipe : RecipeModel {}
