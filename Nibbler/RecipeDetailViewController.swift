//
//  RecipeDetailViewController.swift
//  Nibbler
//
//  Created by Tim Harrison on 24/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

struct RecipeDetailViewModel {
    let title: String
    let ingredients: String
    let method: String
    let notes: String
    let attributedNotes: NSMutableAttributedString
    let imageURL: URL?
    let sourceURL: URL?
}

protocol RecipeDetailDisplay: class {
    
    func display(viewModel: RecipeDetailViewModel)
    func displayShareSheet(for recipeURL: URL)
}

class RecipeDetailViewController: UIViewController {

    @IBOutlet weak var recipeContainerView: UIView!
    @IBOutlet weak var recipeScrollView: UIScrollView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeDetailContainer: RecipeDetailContainerView!
    
    private var bigScreenButton: UIBarButtonItem!
    private let defaultRecipeImage = UIImage(named: "DefaultRecipe")
    private let recipeDetailRouter: RecipeDetailRouter
    private var viewModel: RecipeDetailViewModel?
    private let interactor: RecipeDetailInteractor
    private lazy var imageLoading: ImageLoading = ImageLoading(imageView: self.recipeImage)
    
    init(recipeSave: RecipeWritable, recipe: RecipeModel, recipeDetailRouter: RecipeDetailRouter) {
        self.interactor = RecipeDetailInteractor(recipe: recipe, recipeSave: recipeSave)
        self.recipeDetailRouter = recipeDetailRouter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.extendedLayoutIncludesOpaqueBars = true
        
        recipeDetailContainer.delegate = self
        recipeImage.layer.masksToBounds = true
        recipeImage.layer.cornerRadius = 8
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        self.interactor.configure(with: self)
        self.interactor.fetch()
    }
    
    @objc
    func shareTapped() {
        
        self.interactor.shareTapped()
    }
}

extension RecipeDetailViewController: RecipeDetailDisplay {
    
    func display(viewModel: RecipeDetailViewModel) {
        self.viewModel = viewModel
        self.title = viewModel.title
        self.recipeDetailContainer.ingredientsBodyLabel.text = viewModel.ingredients
        self.recipeDetailContainer.methodBodyLabel.text = viewModel.method
        self.recipeDetailContainer.notesBodyLabel.attributedText = viewModel.attributedNotes
        self.imageLoading.loadImage(imageURL: viewModel.imageURL, placeholderImage: defaultRecipeImage)
    }
    
    func displayShareSheet(for recipeURL: URL) {
        
        self.recipeDetailRouter.presentShareSheet(for: recipeURL)
    }
}

extension RecipeDetailViewController: RecipeDetailDelegate {
    
    func notesButtonTapped() {
        let notesViewController = NotesViewController(notes: viewModel?.notes ?? "")
        notesViewController.delegate = self
        let notesNavigationController = UINavigationController(rootViewController: notesViewController)
        present(notesNavigationController, animated: true)
    }
    
    func originalButtonTapped() {
        guard let recipeURL = viewModel?.sourceURL else {
            return
        }
        recipeDetailRouter.presentRecipeWebViewController(recipeURL: recipeURL)
    }
}

extension RecipeDetailViewController: NotesDelegate {
    
    func didFinishEditing(notes: String) {
        self.interactor.updateNotes(notes: notes)
    }
}
