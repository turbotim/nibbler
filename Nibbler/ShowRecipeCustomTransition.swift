//
//  ShowRecipeAnimatedTransitioning.swift
//  Nibbler
//
//  Created by Tim Harrison on 30/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

enum AnimationDirection {
    case showDetail
    case hideDetail
}

class ShowRecipeCustomTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    let animationDuration = 0.5
    weak var recipeCell: RecipeCollectionViewCell! = nil
    weak var recipeListViewController: RecipeListTransitionDataProvider! = nil
    weak var recipeDetailViewController: RecipeDetailViewController! = nil
    var transitionContext: UIViewControllerContextTransitioning! = nil
    var transitionView: RecipeTransitionView! = nil
    var animationDirection: AnimationDirection = .showDetail
    
    typealias AnimationStep = () -> Void
    
    //MARK - UIViewControllerAnimatedTransitioning
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationDuration
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        self.transitionContext = transitionContext
        
        if transitionContext.viewController(forKey: .to)?.classForCoder == RecipeDetailViewController.classForCoder() {
            
            guard let recipeListViewController = transitionContext.viewController(forKey: .from) as? RecipeListTransitionDataProvider,
                let recipeDetailViewController = transitionContext.viewController(forKey: .to) as? RecipeDetailViewController,
                let transitionIndexPath = recipeListViewController.transitionIndexPath,
                let recipeCell = recipeListViewController.collectionView.cellForItem(at: transitionIndexPath) as? RecipeCollectionViewCell else {
                    assertionFailure("Only supports recipe list and recipe detail")
                    return
            }
            self.recipeListViewController = recipeListViewController
            self.recipeDetailViewController = recipeDetailViewController
            self.recipeCell = recipeCell
            animationDirection = .showDetail
            showDetail()
            
        } else if transitionContext.viewController(forKey: .to)?.classForCoder == RecipeListViewController.classForCoder() {
            
            guard let recipeListViewController = transitionContext.viewController(forKey: .to) as? RecipeListTransitionDataProvider,
                let recipeDetailViewController = transitionContext.viewController(forKey: .from) as? RecipeDetailViewController,
                let transitionIndexPath = recipeListViewController.transitionIndexPath,
                let recipeCell = recipeListViewController.collectionView.cellForItem(at: transitionIndexPath) as? RecipeCollectionViewCell else {
                    assertionFailure("Only supports recipe list and recipe detail")
                    return
            }
            self.recipeListViewController = recipeListViewController
            self.recipeDetailViewController = recipeDetailViewController
            self.recipeCell = recipeCell
            animationDirection = .hideDetail
            hideDetail()
            
        } else {
            assertionFailure("Only supports recipe list and recipe detail")
            return
        }
    }
    
    //MARK - Private Animation methods
    
    private func showDetail() {
        recipeDetailViewController.recipeImage.image = recipeCell.recipeImageView.image
        recipeDetailViewController.view.frame = self.transitionContext.containerView.bounds
        recipeDetailViewController.view.layoutIfNeeded()
        self.transitionView = generateTransitionView()
        configureTransitionViewForSmallSize()
        self.transitionView.alpha = 1.0
        
        UIView.animate(withDuration: animationDuration, delay: 0.0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.3, options: .curveLinear, animations: {
            self.configureTransitionViewForLargeSize()
        }) { (completed) in
            self.transitionContext.containerView.insertSubview(self.recipeDetailViewController.view, belowSubview: self.transitionView)
            self.transitionView.alpha = 0.0
            self.transitionContext.completeTransition(true)
        }
    }
    
    private func hideDetail() {
        transitionView = generateTransitionView()
        configureTransitionViewForLargeSize()
        transitionView.alpha = 1.0
        transitionContext.containerView.insertSubview(self.recipeListViewController.view, belowSubview: self.transitionView)
        transitionContext.containerView.layoutIfNeeded()
        recipeCell.recipeImageView.alpha = 0.0
        
        UIView.animate(withDuration: animationDuration / 4) {
            self.transitionView.detailView.alpha = 0
        }
        
        UIView.animate(withDuration: animationDuration, delay: 0.0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.3, options: .curveLinear, animations: {
            self.configureTransitionViewForSmallSize()
        }) { (completed) in
            self.transitionContext.completeTransition(true)
            //post fade
            self.recipeCell.recipeImageView.alpha = 1.0
            self.transitionView.alpha = 0.0
            self.transitionView.removeFromSuperview()
            self.transitionContext.completeTransition(true)
        }
        UIView.animate(withDuration: animationDuration / 4, delay: animationDuration / 4, options: [], animations: {
            self.transitionView.backgroundView.alpha = 0.0
        })
    }
    
    private func generateTransitionView() -> RecipeTransitionView {
        let transitionView = RecipeTransitionView()
        transitionView.frame = transitionContext.containerView.bounds
        transitionContext.containerView.addSubview(transitionView)
        transitionView.alpha = 0.0
        transitionView.imageView.image = recipeCell.recipeImageView.image
        transitionView.backgroundView.layer.cornerRadius = 8.0
        transitionView.backgroundView.layer.masksToBounds = true
        transitionView.detailView.methodBodyLabel.text = recipeDetailViewController.recipeDetailContainer.methodBodyLabel.text
        transitionView.detailView.ingredientsBodyLabel.text = recipeDetailViewController.recipeDetailContainer.ingredientsBodyLabel.text
        transitionView.detailView.notesBodyLabel.attributedText = recipeDetailViewController.recipeDetailContainer.notesBodyLabel.attributedText

        transitionView.detailView.frame = recipeDetailFrame()
        return transitionView
    }
    
    private func configureTransitionViewForSmallSize() {
        transitionView.imageView.frame = recipeCell.convert(recipeCell.recipeImageView.frame, to: transitionView)
        transitionView.backgroundView.frame = recipeCell.convert(recipeCell.backgroundCardView.frame, to: transitionView)
    }
    
    private func configureTransitionViewForLargeSize() {
        transitionView.backgroundView.frame = contextFrame()
        transitionView.imageView.frame = imageViewFrame()
        var imageFrame = transitionView.imageView.frame
        if imageFrame.origin.y < -imageFrame.height {
            imageFrame.origin.y = -imageFrame.height
        }
        transitionView.imageView.frame = imageFrame
    }
    
    private func contextFrame() -> CGRect {
        let transitionContainerFrame = transitionContext.containerView.bounds
        guard #available(iOS 11.0, *),
            let navigationHeight = recipeDetailViewController.navigationController?.navigationBar.frame.maxY  else {
                return transitionContainerFrame
        }
        let updatedY = transitionContainerFrame.origin.y + navigationHeight
        let updatedHeight = transitionContainerFrame.height - navigationHeight
        return CGRect(x: transitionContainerFrame.origin.x, y: updatedY, width: transitionContainerFrame.width, height: updatedHeight)
    }
    
    private func recipeDetailFrame() -> CGRect {
        var recipeDetailHeight = recipeDetailViewController.recipeContainerView.convert(recipeDetailViewController.recipeDetailContainer.frame, to: transitionView)
        let navigationHeight = recipeDetailViewController.navigationController?.navigationBar.frame.maxY ?? 0
        guard #available(iOS 11.0, *) else {
            switch animationDirection {
            case .showDetail:
                recipeDetailHeight.origin.y = recipeDetailHeight.origin.y + navigationHeight
                return recipeDetailHeight
            case .hideDetail:
                return recipeDetailHeight
            }
        }
        guard animationDirection == .hideDetail else {
            return recipeDetailHeight
        }
        recipeDetailHeight.origin.y = recipeDetailHeight.origin.y - navigationHeight
        return recipeDetailHeight
    }
    
    private func imageViewFrame() -> CGRect {
        var imageViewFrame = recipeDetailViewController.recipeContainerView.convert(recipeDetailViewController.recipeImage.frame, to: transitionView)
        guard #available(iOS 11.0, *),
            animationDirection == .showDetail,
            let navigationHeight = recipeDetailViewController.navigationController?.navigationBar.frame.maxY  else {
                return imageViewFrame
        }
        imageViewFrame.origin.y = imageViewFrame.origin.y + navigationHeight
        return imageViewFrame
    }
}


