//
//  RecipeAccess.swift
//  Nibbler
//
//  Created by Tim Harrison on 29/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit
import CoreData

protocol RecipeReadable {
    func fetchAll(completion: @escaping ([RecipeModel]?) -> Void)
}

protocol RecipeWritable {
    func save()
    func store(webRecipe: WebRecipe) -> RecipeModel?
    func delete(recipe: RecipeModel)
    func registerForNewRecipeUpdates(recipeAddtionCompletion: @escaping () -> ())
}

class RecipeAccess {
    
    static let shared = RecipeAccess()
    
    private init () {}
    
    fileprivate var recipeAdditionObservers = [() -> ()]()
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Nibbler")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
}


extension RecipeAccess: RecipeReadable {
    
    func fetchAll(completion: @escaping ([RecipeModel]?) -> Void) {
        let viewContext = persistentContainer.viewContext
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let recipes = try viewContext.fetch(Recipe.fetchRequest()) as [Recipe]
                let invertedRecipes = recipes.reversed() as [Recipe]
                completion(invertedRecipes)
            } catch {
                completion(nil)
            }
        }
    }
}

extension RecipeAccess: RecipeWritable {
    
    func save() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func store(webRecipe: WebRecipe) -> RecipeModel? {
        
        let savingContext = persistentContainer.newBackgroundContext()
        let recipe = Recipe(context: savingContext)
        
        recipe.imageURL = webRecipe.imageURL?.absoluteString
        recipe.ingredients = webRecipe.ingredientsText
        recipe.method = webRecipe.methodText
        recipe.sourceName = webRecipe.source
        recipe.sourceURL = webRecipe.sourceURL.absoluteString
        recipe.title = webRecipe.title
        if let cookTime = webRecipe.cookTime {
            recipe.cookTime = cookTime
        }
        
        do {
            try savingContext.save()
        } catch {
            return nil
        }
        
        for recipeAdditionObserver in recipeAdditionObservers {
            recipeAdditionObserver()
        }
        
        return recipe
    }
    
    func delete(recipe: RecipeModel) {
        guard let recipeMO = recipe as? Recipe else {
            return
        }
        let viewContext = persistentContainer.viewContext
        viewContext.delete(recipeMO)
        do {
            try viewContext.save()
        } catch {
            assertionFailure("Error deleting")
            return
        }
    }
    
    func registerForNewRecipeUpdates(recipeAddtionCompletion: @escaping () -> ()) {
        recipeAdditionObservers.append(recipeAddtionCompletion)
    }
}
