//
//  Recipe.swift
//  Nibbler
//
//  Created by Tim Harrison on 07/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import Foundation
import CoreData

class Recipe: NSManagedObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Recipe> {
        return NSFetchRequest<Recipe>(entityName: "Recipe")
    }
    
    @NSManaged var cookTime: Double
    @NSManaged var imageURL: String?
    @NSManaged var ingredients: String?
    @NSManaged var method: String?
    @NSManaged var notes: String?
    @NSManaged var recipeJson: String?
    @NSManaged var sourceName: String?
    @NSManaged var sourceURL: String?
    @NSManaged var title: String?
}

