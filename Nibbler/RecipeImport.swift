//
//  RecipeImport.swift
//  Nibbler
//
//  Created by Tim Harrison on 18/11/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import Foundation

class RecipeImport {
    
    private let recipeQueue = RecipeQueue()
    private let recipeAccess: RecipeWritable
    private let parseService = ParseService()
    
    init(recipeAccess: RecipeWritable) {
        
        self.recipeAccess = recipeAccess
    }
    
    func importRecipes() {
        
        for recipe in self.recipeQueue.recipesToStore() {
            
            _ = self.recipeAccess.store(webRecipe: recipe)
        }
        self.recipeQueue.clear()
    }
}
