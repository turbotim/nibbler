//
//  RecipeDetailRouter.swift
//  Nibbler
//
//  Created by Tim Harrison on 31/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class RecipeDetailRouter {

    private let recipeAccess: RecipeAccess
    private weak var presentingViewController: UIViewController? = nil
    
    init(recipeAccess: RecipeAccess) {
        self.recipeAccess = recipeAccess
    }
    
    func attach(viewController: UIViewController) {
        self.presentingViewController = viewController
    }
    
    func presentRecipeWebViewController(recipeURL: URL) {
        let recipeWebView = RecipeWebViewController(url: recipeURL, showsSaveButton: false, recipeAccess: recipeAccess)
        presentingViewController?.present(recipeWebView, animated: true)
    }
    
    func presentShareSheet(for recipeURL: URL) {
        let items = [recipeURL]
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        presentingViewController?.present(activityViewController, animated: true)
    }
}
