//
//  ShowRecipeCustomTransitionDelegate.swift
//  Nibbler
//
//  Created by Tim Harrison on 29/06/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

class ShowRecipeCustomTransitionDelegate: NSObject, UINavigationControllerDelegate {

    public func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if toVC.classForCoder == RecipeListViewController.classForCoder() ||
            toVC.classForCoder == RecipeDetailViewController.classForCoder() {
            
            return ShowRecipeCustomTransition()
        } else {
            return nil
        }
    }
}
