//
//  EmptyLibraryView.swift
//  Brewing
//
//  Created by Tim Harrison on 25/10/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import UIKit

class EmptyLibraryView: UIView {

    private let label = UILabel(frame: .zero)
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        
        self.addSubview(self.label)
        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.pin(innerView: self.label, leadingPadding: 10, trailingPadding: -10)
        self.label.text = "No recipes yet, hit the + button to start adding recipes 🥗🍴"
        self.label.numberOfLines = 0
        self.label.font = .preferredFont(forTextStyle: .title2)
        self.label.textAlignment = .center
        self.backgroundColor = .clear
    }
    
    enum EmptyState {
        case noRecipes
        case filteredReciped
    }
    func configure(for emptyState: EmptyState) {
        
        switch emptyState {
        case .noRecipes:
            self.label.text = "No recipes yet 🙁\n\nAdd a recipe by hitting the + button\nor\nShare directly from Safari to Food Lab\n\n🥗🍴"
        case .filteredReciped:
            self.label.text = "None of your saved recipes match your search \n\n🥗🍴"
        }
    }
}
