//
//  RecipeDetailPresenter.swift
//  Nibbler
//
//  Created by Tim Harrison on 31/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

protocol RecipeDetailInteractorInput {
    
    func fetch()
    func configure(with display: RecipeDetailDisplay)
    func updateNotes(notes: String)
    func shareTapped()
}

class RecipeDetailInteractor: RecipeDetailInteractorInput {
    
    private weak var displayOutput: RecipeDetailDisplay?
    private var recipe: RecipeModel
    private let recipeSave: RecipeWritable
    
    init(recipe: RecipeModel, recipeSave: RecipeWritable) {
        self.recipe = recipe
        self.recipeSave = recipeSave
    }
    
    func fetch() {
        let title = recipe.title ?? ""
        let ingredients = recipe.ingredients ?? ""
        let method = recipe.method ?? ""
        let notes = recipe.notes ?? ""
        let attributedNotes = self.attribute(notes: notes)
        let imageURL: URL?
        if let imageURLString = recipe.imageURL {
            imageURL = URL(string: imageURLString)
        } else {
            imageURL = nil
        }
        let sourceURL: URL?
        if let sourceURLString = recipe.sourceURL, let recipeSourceURL = URL(string: sourceURLString) {
            sourceURL = recipeSourceURL
        } else {
            sourceURL = nil
        }
        let viewModel = RecipeDetailViewModel(title: title,
                                              ingredients: ingredients,
                                              method: method,
                                              notes: notes,
                                              attributedNotes: attributedNotes,
                                              imageURL: imageURL,
                                              sourceURL: sourceURL)
        self.displayOutput?.display(viewModel: viewModel)
    }
    
    func configure(with displayOutput: RecipeDetailDisplay) {
        self.displayOutput = displayOutput
    }
    
    func shareTapped() {
        
        guard let sourceURL = self.recipe.sourceURL,
              let recipeURL = URL(string: sourceURL) else {
            
            return
        }
        self.displayOutput?.displayShareSheet(for: recipeURL)
    }
    
    private func attribute(notes: String?) -> NSMutableAttributedString {
        let tapToEdit = " Edit"
        let originalNotes: String
        if let recipeNotes = notes, recipeNotes.count > 0 {
            originalNotes = recipeNotes
        } else {
            originalNotes = "Add your notes here 😀"
        }
        let linkBlue = UIColor(red: 0, green: 0.478431, blue: 1, alpha: 1.0)
        let attributedNotes = NSMutableAttributedString(string: "\(originalNotes)\(tapToEdit)")
        let buttonFont = UIFont.systemFont(ofSize: 15) as Any
        attributedNotes.addAttribute(.foregroundColor, value: linkBlue, range: NSRange(location: attributedNotes.length - tapToEdit.count, length: tapToEdit.count))
        attributedNotes.addAttribute(.font, value: buttonFont, range: NSRange(location: attributedNotes.length - tapToEdit.count, length: tapToEdit.count))
        return attributedNotes
    }
    
    func updateNotes(notes: String) {
        recipe.notes = notes
        self.fetch()
        recipeSave.save()
    }
}
