//
//  RecipeParser.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 02/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation

protocol Parser {
    
    func parse(html:String, source:URL) -> WebRecipe?
}

class RecipeProvider {

    let parserProvider = ParserProvider()
    
    func canParse(url:URL) -> Bool {
        
        return parserProvider.parser(forURL: url) != nil
    }
    
    func recipe(fromHtml html:String, url:URL) -> WebRecipe? {
        
        guard let parser = parserProvider.parser(forURL: url) else {
        
            return nil
        }
        
        return parser.parse(html: html, source:url)
    }
}
