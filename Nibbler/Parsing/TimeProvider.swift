//
//  TimeProvider.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 04/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation

class TimeProvider {
    
    func isHour(text:String) -> Bool {
        return text.lowercased() == "hour" || text.lowercased() == "hours"
    }
    
    func isMinute(text:String) -> Bool {
        return text.lowercased() == "minute" || text.lowercased() == "minutes" || text.lowercased() == "min" || text.lowercased() == "mins"
    }

    func time(fromText text:String) -> TimeInterval? {
        
        var unit:String?
        var number:Int?
        
        for word in text.components(separatedBy: .whitespacesAndNewlines) {
            
            if word == "" {
                continue
            }
            
            let lastWord = word.components(separatedBy: "-").last!
            
            let parsedCharacters = lastWord.characters.filter {
                
                return "0123456789:".characters.contains($0)
            }
            
            if parsedCharacters.contains(":") {
                
                let hoursMinutes = parsedCharacters.split(separator: ":")
                if hoursMinutes.count != 2 {
                    continue
                }
                
                if let hours = Int(String(hoursMinutes.first!)),
                    let mins = Int(String(hoursMinutes.last!)) {
                    
                    number = (hours * 60) + mins
                    return TimeInterval(number!)
                }
                
            } else {
            
                let numbers = String(parsedCharacters)
                
                if let _ = Int(numbers) {
                    number = Int(numbers)
                } else if isHour(text: word) || isMinute(text: word) {
                    unit = word
                }
            }
        }
        
        if var number = number,
            let unit = unit {
            
            if isHour(text:unit) {
                number = number * 60
            }
            
            return TimeInterval(number)
        }
            
        return nil
    }
    
    func totalTime(prepTimeText:String?, cookTimeText:String?) -> TimeInterval? {
        
        if prepTimeText == nil && cookTimeText == nil {
            return nil
        }
        
        var totalTime:TimeInterval = 0
        
        if let cookTimeText = cookTimeText,
            let cookTime = time(fromText: cookTimeText) {
            
            totalTime = cookTime
        }
        
        if let prepTimeText = prepTimeText,
            let prepTime = time(fromText: prepTimeText) {
            
            totalTime += prepTime
        }
        
        if totalTime > 0 {
            return totalTime
        }
        
        return nil
    }
}
