//
//  ParserConfiguration.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 02/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation

class ParserProvider {

    let parsers:[String:Parser] = [
        "www.bbc.co.uk": BBCParser(),
        "www.jamieoliver.com": JamieOliverParser(),
        "www.taste.com.au": TasteParser(),
    ]
    
    func parser(forURL url:URL) -> Parser? {
        
        guard let host = url.host else {
            return nil
        }
        
        if let parser = parsers[host] {
            return parser
        }
        
        return nil
    }
}
