//
//  JamieOliverParser.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 04/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import Kanna

class JamieOliverParser: Parser {

    let htmlProvider = HTMLProvider()
    
    public func parse(html:String, source:URL) -> WebRecipe? {
        
        guard let doc = htmlProvider.doc(html: html),
            let ingredientsParentDiv = htmlProvider.ingredientsParentDiv(doc: doc),
            let methodParentDiv = htmlProvider.methodParentDiv(doc: doc),
            let ingredientSections = htmlProvider.sections(parentDiv: ingredientsParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            let methodSections = htmlProvider.sections(parentDiv: methodParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            let recipeTitle = htmlProvider.text(fromDoc: doc, tag: "h1", htmlClass: nil).first else {
                
                return nil
        }
        
        let cookTime = time(doc: doc)
        let imageURL = htmlProvider.image(doc: doc, alt:recipeTitle)
        
        return WebRecipe(title: recipeTitle, method: methodSections, ingredients: ingredientSections, cookTime: cookTime, sourceURL: source, source: "Jamie Oliver", imageURL: imageURL)
    }
    
    func time(doc:HTMLDocument) -> TimeInterval? {
        
        let timeProvider = TimeProvider()
        let cookTime = htmlProvider.text(fromDoc: doc, tag: "div", htmlClass: "recipe-detail time").first
        return timeProvider.totalTime(prepTimeText: nil, cookTimeText: cookTime)
        
    }
}
