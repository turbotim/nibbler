//
//  Recipe.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 10/02/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation

class WebRecipe {

    let cookTime:TimeInterval?
    let ingredients:[String:[String]]
    let method:[String:[String]]
    let title:String
    let source: String
    let sourceURL:URL
    let imageURL:URL?
    
    var ingredientsText: String {
        get {
            return text(sections: ingredients)
        }
    }
    
    var methodText: String {
        get {
            return text(sections: method)
        }
    }
    
    init(title:String, method:[String:[String]], ingredients:[String:[String]], cookTime:TimeInterval?, sourceURL:URL, source:String, imageURL:URL?) {
        
        self.cookTime = cookTime
        self.ingredients = ingredients
        self.method = method
        self.title = title
        self.source = source
        self.sourceURL = sourceURL
        self.imageURL = imageURL
    }
    
    func text(sections:[String:[String]]) -> String {
    
        var text = ""
        for section in sections {
            
            let sectionTitle = section.key
            
            if sectionTitle.characters.count > 0 {
                text.append("\(sectionTitle)\n")
            }
            
            var itemCount = 0
            
            for item in section.value {
                
                if itemCount == 0 {
                    text.append("\n")
                } else {
                    text.append("\n\n")
                }
                text.append("• \(item)")
                itemCount = itemCount + 1
            }
        }
        return text
    }
    
    func ingredientsHTML() -> String {
        
        var html = "<h2>Ingredients</h2>\n"
        
        for section in ingredients {
            
            html.append("<h3>\(section.key)</h3>\n")
            html.append("<ul>\n")
            for text in section.value {
                html.append("<li>\(text)</li>\n")
            }
            html.append("</ul>\n")
        }
        
        return html
    }
    
    func methodHTML() -> String {
        
        var html = "<h2>Method</h2>\n"
        
        for section in method {
            
            html.append("<h3>\(section.key)</h3>\n")
            html.append("<ol>\n")
            for text in section.value {
                html.append("<li>\(text)</li>\n")
            }
            html.append("</ol>\n")
        }
        
        return html
    }
    
    func recipeHTML() -> String {
        
        return "\(ingredientsHTML())\(methodHTML())"
    }
}
