//
//  BBCParser.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 03/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import Kanna

class BBCParser: Parser {
    
    let htmlProvider = HTMLProvider()
    
    public func parse(html:String, source:URL) -> WebRecipe? {
    
        guard let doc = htmlProvider.doc(html: html),
            let ingredientsParentDiv = htmlProvider.ingredientsParentDiv(doc: doc),
            let methodParentDiv = htmlProvider.methodParentDiv(doc: doc),
            let ingredientSections = htmlProvider.sections(parentDiv: ingredientsParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            let methodSections = htmlProvider.sections(parentDiv: methodParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            let recipeTitle = htmlProvider.text(fromDoc: doc, tag: "h1", htmlClass: "content-title__text").first else {
                
                return nil
        }
        
        let cookTime = time(doc: doc)
        let imageURL = image(doc: doc)
        
        return WebRecipe(title: recipeTitle, method: methodSections, ingredients: ingredientSections, cookTime: cookTime, sourceURL: source, source: "BBC Food", imageURL: imageURL)
    }
    
    func image(doc:HTMLDocument) -> URL? {
        
        if let src = htmlProvider.attributes(fromDoc: doc, tag: "img", htmlClass: "recipe-media__image", attribute: "src").first {
            return URL(string:src)
        }
        
        return nil
    }
    
    func time(doc:HTMLDocument) -> TimeInterval? {
        
        let timeProvider = TimeProvider()
        
        let prepTime = htmlProvider.text(fromDoc: doc, tag: "p", htmlClass: "recipe-metadata__prep-time").first
        let cookTime = htmlProvider.text(fromDoc: doc, tag: "p", htmlClass: "recipe-metadata__cook-time").first
        
        return timeProvider.totalTime(prepTimeText: prepTime, cookTimeText: cookTime)

    }
}
