//
//  DeleteRecipeView.swift
//  Nibbler
//
//  Created by Tim Harrison on 01/07/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit


protocol DeleteRecipeDelegate: class {
    func deleteTapped(recipeView: DeleteRecipeView)
}

class DeleteRecipeView: UIView {

    let titleLabel = UILabel(frame: .zero)
    let tapGestureRecognizer = UITapGestureRecognizer()
    weak var delegate: DeleteRecipeDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .red
        self.addSubview(titleLabel)
        tapGestureRecognizer.addTarget(self, action: #selector(tappedView))
        self.addGestureRecognizer(tapGestureRecognizer)
        titleLabel.textColor = .white
        titleLabel.text = "Delete"
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let views = ["titleLabel":titleLabel]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[titleLabel]", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[titleLabel]|", options: [], metrics: nil, views: views))
    }
    
    @objc func tappedView() {
        if let delegate = delegate {
            delegate.deleteTapped(recipeView: self)
        }
    }
}
