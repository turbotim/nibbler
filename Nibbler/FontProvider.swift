//
//  FontProvider.swift
//  Nibbler
//
//  Created by Tim Harrison on 02/12/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

struct FontProvider {
    static let titleFont = UIFont(name: "HelveticaNeue-Bold", size: 24)
    static let bodyFont = UIFont(name: "HelveticaNeue-Light", size: 16)
}
