//
//  UIView+Pin.swift
//  Brewing
//
//  Created by Tim Harrison on 07/03/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

extension UIView {

    func pin(innerView: UIView,
             leadingPadding: CGFloat = 0,
             topPadding: CGFloat = 0,
             trailingPadding: CGFloat = 0,
             bottomPadding: CGFloat = 0) {
        
        innerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            innerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leadingPadding),
            innerView.topAnchor.constraint(equalTo: topAnchor, constant: topPadding),
            innerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: trailingPadding),
            innerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: bottomPadding)
            ])
    }
}
