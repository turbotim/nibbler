//
//  Upgrade.swift
//  Nibbler
//
//  Created by Tim Harrison on 31/10/2020.
//  Copyright © 2020 Tim Harrison. All rights reserved.
//

import UIKit

class Upgrade {

    private let migrationVersionKey = "com.nibbler.migrationVersion"
    private let currentVersion: Float = 2.0
    private let updateQueue = DispatchQueue(label: "com.nibbler.updateQueue")
    
    func performMigrationIfNeeded() {
        
        let userDefaults = UserDefaults.standard
            
        let migrationVersion = userDefaults.float(forKey: self.migrationVersionKey)
        
        guard migrationVersion < self.currentVersion else { return }
        
        self.performMigration() {
            
            userDefaults.set(self.currentVersion, forKey: self.migrationVersionKey)
        }
    }
    
    private func performMigration(completion: @escaping () -> Void) {
        
        let recipeAccess = RecipeAccess.shared
        recipeAccess.fetchAll { [weak self] recipeModels in
            
            guard let recipeModels = recipeModels,
                let strongSelf = self else { return }
            
            strongSelf.updateQueue.async {
                
                let dispatchGroup = DispatchGroup()
                
                for recipeModel in recipeModels {
                    
                    guard let sourceURLString = recipeModel.sourceURL,
                        let sourceURL = URL(string: sourceURLString),
                        recipeModel.imageURL == nil else { continue }
                    
                    dispatchGroup.enter()
                    
                    strongSelf.recipeImageURL(for: sourceURL) { imageURL in
                                                
                                                var mutableRecipeModel = recipeModel
                                                mutableRecipeModel.imageURL = imageURL?.absoluteString
                                                dispatchGroup.leave()
                    }
                    
                    dispatchGroup.wait()
                }
                
                recipeAccess.save()
                completion()
            }
        }
    }
    
    private func recipeImageURL(for webURL: URL,
                                completion: @escaping (URL?) -> Void) {
        
        let task = URLSession.shared.dataTask(with: webURL) { (data, urlResponse, error) in
            
            let recipeProvider = RecipeProvider()
            guard let data = data,
                let html = String(data: data, encoding: .utf8) else {
                
                completion(nil)
                return
            }
            let parserResult = recipeProvider.recipe(fromHtml: html, url: webURL)
            completion(parserResult.webRecipe?.imageURL)
        }
        task.resume()
    }
}
