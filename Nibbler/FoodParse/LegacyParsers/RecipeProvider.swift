//
//  RecipeParser.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 02/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation

protocol Parser {
    
    func parse(html:String, source:URL) -> ParserResult
}

public struct ParserResult {
    public let webRecipe: WebRecipe?
    public let links: [String]
}

public class RecipeProvider {

    let parserProvider = ParserProvider()
    
    public init() {
        
    }
    
    public func canParse(url:URL) -> Bool {
        
        return parserProvider.parser(forURL: url) != nil
    }
    
    public func recipe(fromHtml html:String, url:URL) -> ParserResult {
    
        guard let parser = parserProvider.parser(forURL: url) else {
            return ParserResult(webRecipe: nil, links: [])
        }
        
        return parser.parse(html: html, source:url)
    }
}
