//
//  FoodScrape.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 10/02/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import Kanna

enum ParseState {
    case start
    case ingredients
    case method
    case end
}

enum TagType: Int, Comparable {
    
    case div, a, p, h4, h3, h2, h1
    
    init?(tag:String) {
        switch tag.lowercased() {
        case "div":
            self = .div
        case "a":
            self = .a
        case "p":
            self = .p
        case "h4":
            self = .h4
        case "h3":
            self = .h3
        case "h2":
            self = .h2
        case "h1":
            self = .h1
        default:
            return nil
        }
    }
    
    public static func <(lhs: TagType, rhs: TagType) -> Bool {
        
        return lhs.rawValue < rhs.rawValue
    }
    
}

class HTMLProvider {
    
    let ingredientsTitles = ["Ingredients"]
    let methodTitles = ["Method", "Directions", "Preparation", "Instructions"]
    
    public func doc(html:String) -> HTMLDocument? {
        
        return try? HTML(html: html, encoding: .utf8)
    }
    
    public func sections(parentDiv:Kanna.XMLElement, sectionHeadingTag:String, valueTag:String) -> [RecipeSection]? {
        return sections(parentDiv: parentDiv, sectionHeadingTag: sectionHeadingTag, valueTag: valueTag, htmlClass: nil)
    }
    
    public func links(doc: HTMLDocument, validHost: String?) -> [String] {
        var links = [String]()
        for element in doc.css("*") {
            guard let tagName = element.tagName,
                tagName == "a",
                let link = element["href"] else {
                continue
            }
            
            if let validHost = validHost {
                if link.range(of: validHost) != nil {
                    links.append(link)
                }
            } else {
                links.append(link)
            }
        }
        
        return links
    }
    
    private func isHeading(text:String) -> Bool{
        for title in methodTitles {
            if title.lowercased() == text.lowercased().trimmingCharacters(in: .whitespaces) {
                return false
            }
        }
        for title in ingredientsTitles {
            if title.lowercased() == text.lowercased().trimmingCharacters(in: .whitespaces) {
                return false
            }
        }
        return true
    }
    
    public func sections(parentDiv:Kanna.XMLElement, sectionHeadingTag:String, valueTag:String, htmlClass:String?) -> [RecipeSection]? {
        
        var sections = [RecipeSection]()
        var sectionName = ""
        var sectionValues = [String]()
        
        for element in parentDiv.css("*") {
            
            guard let tagName = element.tagName else {
                continue
            }
            
            if tagName == sectionHeadingTag {
                
                if sectionValues.count > 0 {
                    sections.append(RecipeSection(title: sectionName, body: sectionValues))
                }
                
                if let elementName = element.text, isHeading(text: elementName) {
                    sectionName = elementName
                } else {
                    sectionName = ""
                }
                
                sectionValues = [String]()
                
            } else if tagName == valueTag {
                
                let sectionValue = cleanElementText(element: element)
                
                if let htmlClass = htmlClass {
                    
                    guard let elementClass = element.className else {
                        continue
                    }
                    
                    if elementClass == htmlClass && !sectionValue.isEmpty {
                        sectionValues.append(sectionValue)
                    }
                    
                } else if (!sectionValue.isEmpty) {
                
                    sectionValues.append(sectionValue)
                }
            }
        }
        
        if (sectionValues.count > 0) {
            sections.append(RecipeSection(title: sectionName, body: sectionValues))
        }
        
        return sections
    }
    
    public func cleanElementText(element: Kanna.XMLElement) -> String {
        
        let filtered = element.text!.unicodeScalars.filter { !CharacterSet.newlines.contains($0) }
        let noNewLines = String(String.UnicodeScalarView(filtered))
        var reducedSpaces = noNewLines.trimmingCharacters(in: .whitespaces)
        
        while true {
            let updatedString = reducedSpaces.replacingOccurrences(of: "  ", with: " ").replacingOccurrences(of: " ,", with: ",")
            if updatedString == reducedSpaces {
                break
            }
            reducedSpaces = updatedString
        }
        
        return reducedSpaces
        
//        return element.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    public func image(doc:HTMLDocument, alt:String) -> URL? {
        
        for element in doc.css("img") {
            if let imgAlt = element["alt"],
                let src = element["src"] {
                
                if alt.lowercased().trimmingCharacters(in: .whitespaces) == imgAlt.lowercased().trimmingCharacters(in: .whitespaces) {
                    return URL(string:src)
                }
            }
        }
        return nil
    }
    
    public func ingredientsParentDiv(doc:HTMLDocument) -> Kanna.XMLElement? {
        
        if let ingredientsTitleElement = titleElement(doc: doc, titles: ingredientsTitles),
            let ingredientsParentDiv = parentDiv(element: ingredientsTitleElement) {
            return  ingredientsParentDiv
        }
        
        return nil
    }
    
    public func methodParentDiv(doc:HTMLDocument) -> Kanna.XMLElement? {
        
        if let methodTitleElement = titleElement(doc: doc, titles: methodTitles),
            let methodParentDiv = parentDiv(element: methodTitleElement) {
            return methodParentDiv
        }
        
        return nil
    }
    
    public func elements(fromDoc doc:HTMLDocument, tag:String, htmlClass:String) -> [Kanna.XMLElement] {
        
        var elements = [Kanna.XMLElement]()
        
        for element in doc.css("\(tag)") {
            
            if element.className?.contains(htmlClass) ?? false {
                elements.append(element)
            }
        }
        
        return elements
    }
    
    public func elements(fromDoc doc:HTMLDocument, tag:String) -> [Kanna.XMLElement] {
        
        var elements:[Kanna.XMLElement]?
        
        for element in doc.css("\(tag)") {
            
            if elements == nil {
                elements = [element]
            } else {
                elements!.append(element)
            }
        }
        
        if let elements = elements {
            return elements
        }
        
        return []
    }
    
    public func attributes(fromDoc doc:HTMLDocument, tag:String, htmlClass:String, attribute:String) -> [String] {
        
        var attributes = [String]()
            
        for element in elements(fromDoc: doc, tag: tag, htmlClass: htmlClass) {
        
            if let attributeValue = element[attribute]  {
                attributes.append(attributeValue)
            }
        }
        
        return attributes
    }
    
    public func text(fromDoc doc:HTMLDocument, tag:String, htmlClass:String?) -> [String] {
        
        var textFromElements = [String]()
        let matchingElements:[Kanna.XMLElement]
        
        if let htmlClass = htmlClass {
            matchingElements = elements(fromDoc: doc, tag: tag, htmlClass: htmlClass)
        } else {
            matchingElements = elements(fromDoc: doc, tag: tag)
        }
        
        for element in matchingElements {
            if let text = element.text {
                textFromElements.append(text)
            }
        }
        
        return textFromElements
    }
    
    public func remove(tag: String, element:Kanna.XMLElement) -> Kanna.XMLElement? {
        
        for tagElement in element.css("\(tag)") {
            element.removeChild(tagElement)
        }
        
        return element
    }
    
    // PRIVATE

    private func parentDiv(element:Kanna.XMLElement) -> Kanna.XMLElement? {
        
        guard let parent = element.parent,
            let tagName = parent.tagName else {
                return nil
        }
        
        if tagName == "div" || tagName == "section" {
            return parent
        } else {
            return parentDiv(element: parent)
        }
    }
    
    private func trim(_ text:String) -> String {
        
        var textToTrim = text
        
        if let firstText = text.components(separatedBy: .newlines).first {
            textToTrim = firstText
        }
        
        //I know - but trust me
        return textToTrim.trimmingCharacters(in: .newlines).trimmingCharacters(in: .whitespaces).trimmingCharacters(in: .newlines).trimmingCharacters(in: .whitespaces)
    }
    
    private func titleElement(doc: HTMLDocument, titles: [String]) -> Kanna.XMLElement? {
        
        for title in titles {
            
            if let titleElement = titleElement(doc: doc, title: title) {
                return titleElement
            }
        }
        
        return nil
    }
    
    private func titleElement(doc: HTMLDocument, title: String) -> Kanna.XMLElement? {
        
        var currentTitleElement: Kanna.XMLElement?
        
        for element in doc.css("*") {
            
            guard let text = element.text,
                let tagName = element.tagName,
                let titleTag = TagType(tag: tagName) else {
                    continue
            }
            
            if trim(text).lowercased() == title.lowercased() {
                
                guard let currentTitle = currentTitleElement,
                    let currentTitleTagName = currentTitle.tagName,
                    let currentTitleTag = TagType(tag:currentTitleTagName) else {
                        
                        currentTitleElement = element
                        continue
                }
                
                if titleTag > currentTitleTag {
                    
                    currentTitleElement = element
                }
            }
            
        }
        
        return currentTitleElement
    }
}


