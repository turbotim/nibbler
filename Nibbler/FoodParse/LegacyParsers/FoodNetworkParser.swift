//
//  FoodNetworkParser.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 21/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import Kanna

class FoodNetworkParser: Parser {
    
    let htmlProvider = HTMLProvider()
    
    public func parse(html:String, source:URL) -> ParserResult {
        
        guard let doc = htmlProvider.doc(html: html),
            let ingredientsParentDiv = htmlProvider.elements(fromDoc: doc, tag: "div", htmlClass: "o-Ingredients__m-Body").first,
            let methodParentDiv = htmlProvider.elements(fromDoc: doc, tag: "div", htmlClass: "o-Method__m-Body").first,
            let cleanMethodParentDiv = htmlProvider.remove(tag: "a", element: methodParentDiv),
            let ingredientSections = htmlProvider.sections(parentDiv: ingredientsParentDiv, sectionHeadingTag: "h3", valueTag: "label"),
            let methodSections = htmlProvider.sections(parentDiv: cleanMethodParentDiv, sectionHeadingTag: "h3", valueTag: "p"),
            let recipeTitle = htmlProvider.text(fromDoc: doc, tag: "h1", htmlClass: nil).first else {
                
                return ParserResult(webRecipe: nil, links: [])
        }
        
        let cookTime = time(doc: doc)
        let imageURL = htmlProvider.image(doc: doc, alt:recipeTitle)
        let rawOtherText = htmlProvider.text(fromDoc: doc, tag: "div", htmlClass: "recipe-description") .first ?? ""
        
       let otherText = [RecipeSection(title: nil, body: [rawOtherText])]
        
        let recipe = WebRecipe(title: recipeTitle, method: methodSections, ingredients: ingredientSections, otherText: otherText, cookTime: cookTime, sourceURL: source, source: "Food Network", imageURL: imageURL)
        return ParserResult(webRecipe: recipe, links: [])
    }
    
    func time(doc:HTMLDocument) -> TimeInterval? {
        
        guard let timeText = htmlProvider.text(fromDoc: doc, tag: "dd", htmlClass: "o-RecipeInfo__a-Description--Total").first else {
                return nil
        }
        
        let timeProvider = TimeProvider()
        return timeProvider.totalTime(prepTimeText: nil, cookTimeText: timeText)
    }
    
}
