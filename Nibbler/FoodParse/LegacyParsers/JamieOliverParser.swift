//
//  JamieOliverParser.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 04/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import Kanna

class JamieOliverParser: Parser {

    let htmlProvider = HTMLProvider()
    
    public func parse(html:String, source:URL) -> ParserResult {
        
        guard let doc = htmlProvider.doc(html: html) else {
            return ParserResult(webRecipe: nil, links: [])
        }
        let links = htmlProvider.links(doc: doc, validHost: "www.jamieoliver.com")
        
        guard let ingredientsParentDiv = htmlProvider.ingredientsParentDiv(doc: doc),
            let methodParentDiv = htmlProvider.methodParentDiv(doc: doc),
            let ingredientSections = htmlProvider.sections(parentDiv: ingredientsParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            var methodSections = htmlProvider.sections(parentDiv: methodParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            let recipeTitle = htmlProvider.text(fromDoc: doc, tag: "h1", htmlClass: nil).first else {
                
                return ParserResult(webRecipe: nil, links: links)
        }
        
        if methodSections.count == 0, let alternativeMethodSections = htmlProvider.sections(parentDiv: methodParentDiv, sectionHeadingTag: "h3", valueTag: "div") {
            methodSections = alternativeMethodSections
        }
        
        let cookTime = time(doc: doc)
        let imageURL = image(doc: doc)
        var finalImageURL: URL? = nil
        if let imageString = imageURL?.absoluteString {
            finalImageURL = URL(string: fix(url:imageString))
        }
        
        var rawOtherText = htmlProvider.text(fromDoc: doc, tag: "div", htmlClass: "container recipe-container") .first ?? ""
        rawOtherText = rawOtherText.removingHTMLTags()
        rawOtherText = rawOtherText.seperateComma()
        
        for section in methodSections {
            for text in section.body {
                rawOtherText = rawOtherText.replacingOccurrences(of: text, with: "")
            }
        }
        
        for section in ingredientSections {
            for text in section.body {
                rawOtherText = rawOtherText.replacingOccurrences(of: text, with: "")
            }
        }
        
        let otherText = [RecipeSection(title: nil, body: [rawOtherText])]
        
        let webRecipe = WebRecipe(title: recipeTitle, method: methodSections, ingredients: ingredientSections, otherText: otherText, cookTime: cookTime, sourceURL: source, source: "Jamie Oliver", imageURL: finalImageURL)
        return ParserResult(webRecipe: webRecipe, links: links)
    }
    
    func time(doc:HTMLDocument) -> TimeInterval? {
        
        let timeProvider = TimeProvider()
        let cookTime = htmlProvider.text(fromDoc: doc, tag: "div", htmlClass: "recipe-detail time").first
        return timeProvider.totalTime(prepTimeText: nil, cookTimeText: cookTime)
    }
    
    public func image(doc:HTMLDocument) -> URL? {
        
        let imgWrappedElements = htmlProvider.elements(fromDoc: doc, tag: "div", htmlClass: "hero-wrapper")
        for element in imgWrappedElements {
            
            for subElement in element.css("img") {
            
                if subElement.tagName == "img", let imageURLString = subElement["src"] {
                    
                    return URL(string: "http:\(imageURLString)")
                }
            }
        }
        
        return nil
    }
    
    func fix(url: String) -> String {
        if url.prefix(upTo: url.index(url.startIndex, offsetBy: 2)) == "//" {
            return "http:\(url)"
        }
        return url
    }
}

extension String {
    
    func seperateComma() -> String {
        return self.replacingOccurrences(of: " ,", with: ",")
    }
    
    func removingHTMLTags() -> String {
        do {
            let attributedString = try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
            return attributedString.string
        } catch {
            print("error:", error)
            return  ""
        }
    }
}
