//
//  GuardianParser.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 04/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import Kanna

class TasteParser: Parser {

    let htmlProvider = HTMLProvider()
    
    public func parse(html:String, source:URL) -> ParserResult {
        
        guard let doc = htmlProvider.doc(html: html),
            let ingredientsParentDiv = htmlProvider.ingredientsParentDiv(doc: doc),
            let methodParentDiv = htmlProvider.methodParentDiv(doc: doc),
            let ingredientSections = htmlProvider.sections(parentDiv: ingredientsParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            let methodSections = htmlProvider.sections(parentDiv: methodParentDiv, sectionHeadingTag: "h3", valueTag: "div", htmlClass: "recipe-method-step-content"),
            let recipeTitle = htmlProvider.text(fromDoc: doc, tag: "h1", htmlClass: nil).first else {
                
                return ParserResult(webRecipe: nil, links: [])
        }
        
        let cookTime = time(doc: doc)
        let imageURL = htmlProvider.image(doc: doc, alt:recipeTitle)
        let rawOtherText: String = htmlProvider.text(fromDoc: doc, tag: "div", htmlClass: "recipe-description").first ?? ""
        let otherText = [RecipeSection(title: nil, body: [rawOtherText])]
        
        let recipe = WebRecipe(title: recipeTitle, method: methodSections, ingredients: ingredientSections, otherText: otherText, cookTime: cookTime, sourceURL: source, source: "Taste", imageURL: imageURL)
        return ParserResult(webRecipe: recipe, links: [])
    }
    
    func timeString(timeText:String, name:String) -> String? {
        
        let components = timeText.components(separatedBy: .newlines)
        
        for component in components {
            if component.lowercased().contains(name.lowercased()) {
                return component
            }
        }
        
        return nil
    }
    
    func time(doc:HTMLDocument) -> TimeInterval? {
        
        guard let timeText = htmlProvider.text(fromDoc: doc, tag: "ul", htmlClass: "recipe-cooking-infos").first,
            let prepTime = timeString(timeText: timeText, name: "prep"),
            let cookTime = timeString(timeText: timeText, name: "cook") else {
                return nil
        }
        
        let timeProvider = TimeProvider()
        return timeProvider.totalTime(prepTimeText: prepTime, cookTimeText: cookTime)
    }
    
}
