//
//  BBCParser.swift
//  ScrapeSomething
//
//  Created by Tim Harrison on 03/03/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import Kanna

class BBCParser: Parser {
    
    let htmlProvider = HTMLProvider()
    var links = [String]()
    
    public func parse(html:String, source:URL) -> ParserResult {
    
        guard let doc = htmlProvider.doc(html: html) else {
            return ParserResult(webRecipe: nil, links: [])
        }
        
        links = htmlProvider.links(doc: doc, validHost: "food/recipes")
        
        guard let ingredientsParentDiv = htmlProvider.ingredientsParentDiv(doc: doc),
            let methodParentDiv = htmlProvider.methodParentDiv(doc: doc),
            let ingredientSections = htmlProvider.sections(parentDiv: ingredientsParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            let methodSections = htmlProvider.sections(parentDiv: methodParentDiv, sectionHeadingTag: "h3", valueTag: "li"),
            let recipeTitle = htmlProvider.text(fromDoc: doc, tag: "h1", htmlClass: "content-title__text").first else {
                return ParserResult(webRecipe: nil, links: links)
        }
        
        var rawOtherText = htmlProvider.text(fromDoc: doc, tag: "div", htmlClass: "recipe-description") .first ?? ""
        
        for section in methodSections {
            for text in section.body {
                rawOtherText = rawOtherText.replacingOccurrences(of: text, with: "")
            }
        }
        
        for section in ingredientSections {
            for text in section.body {
                rawOtherText = rawOtherText.replacingOccurrences(of: text, with: "")
            }
        }
        
        let cookTime = time(doc: doc)
        let imageURL = image(doc: doc)
        
       let otherText = [RecipeSection(title: nil, body: [rawOtherText])]
        
        let recipe = WebRecipe(title: recipeTitle, method: methodSections, ingredients: ingredientSections, otherText: otherText, cookTime: cookTime, sourceURL: source, source: "BBC Food", imageURL: imageURL)
        return ParserResult(webRecipe: recipe, links: links)
    }
    
    func image(doc:HTMLDocument) -> URL? {
        
        let imgWrappedElements = htmlProvider.elements(fromDoc: doc, tag: "div", htmlClass: "recipe-media__image")
        for element in imgWrappedElements {
            
            for subElement in element.css("img") {
            
                if subElement.tagName == "img", let imageURLString = subElement["src"] {
                    
                    return URL(string: imageURLString)
                }
            }
        }
        
        return nil
    }
    
    func time(doc:HTMLDocument) -> TimeInterval? {
        
        let timeProvider = TimeProvider()
        
        let prepTime = htmlProvider.text(fromDoc: doc, tag: "p", htmlClass: "recipe-metadata__prep-time").first
        let cookTime = htmlProvider.text(fromDoc: doc, tag: "p", htmlClass: "recipe-metadata__cook-time").first
        
        return timeProvider.totalTime(prepTimeText: prepTime, cookTimeText: cookTime)

    }
}
