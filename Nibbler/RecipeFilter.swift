//
//  RecipeFilter.swift
//  Nibbler
//
//  Created by Tim Harrison on 08/01/2021.
//  Copyright © 2021 Tim Harrison. All rights reserved.
//

import Foundation

class RecipeFilter {

    var filteredRecipes: [RecipeModel] = []
    private var recipes: [RecipeModel] = []
    private var filterText: String = ""
    
    var noResultsDueToFilters: Bool {
        return self.recipes.count > 0 && self.filteredRecipes.count == 0
    }

    func filter(with text: String) {
        self.filterText = text
        self.filterRecipes()
    }

    func configure(with recipes: [RecipeModel]) {
        self.recipes = recipes
        self.filterRecipes()
    }
    
    func remove(recipe: RecipeModel) {
        self.recipes = self.recipes.filter {
            recipe.sourceURL != $0.sourceURL
        }
        self.filterRecipes()
    }
    
    private func filterRecipes() {
        guard filterText.count > 0 else {
            self.filteredRecipes = recipes
            return
        }
        self.filteredRecipes = self.recipes.filter {
            $0.title?.lowercased().contains(self.filterText.lowercased()) ?? true
        }
    }

}
