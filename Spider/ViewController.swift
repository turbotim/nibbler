//
//  ViewController.swift
//  Spider
//
//  Created by Tim Harrison on 15/09/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Cocoa
import FoodParseMac

class ViewController: NSViewController {
    
    let baseDir = "/Users/timharrison/Desktop/"
    let recipeProvider = RecipeProvider()
    var links: Set<String> = ["http://www.bbc.co.uk/food/recipes/lentilshepherdspiewi_93532"]
//    var links: Set<String> = ["https://www.jamieoliver.com/recipes/fruit-recipes/speedy-steamed-pudding-pots/"]
    var visitedLinks = [String]()
    var successfullyScraped: Int = 0
    let maxScrape = 1000
    
    override func viewDidAppear() {
        super.viewDidAppear()
        deleteExisitingFiles()
        scrapeNext()
    }
    
    func scrapeNext() {
        
        guard var link = links.first else {
            print("No links left")
            return
        }
        for potentialLink in links {
            if potentialLink.contains("recipes") {
                link = potentialLink
                break
            }
        }
        guard let url = URL(string: link) else {
            links.remove(link)
            scrapeNext()
            return
        }
        print("Scraped: \(successfullyScraped) of \(maxScrape)")
        print("Links Remaining: \(links.count), \nCurrent URL: \(url)")
        scrape(url: url) { [weak self] (parserResult) in
            
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.scrapeComplete(parserResult: parserResult, link: link)
            if strongSelf.successfullyScraped < strongSelf.maxScrape {
                DispatchQueue.main.async {
                    self?.scrapeNext()
                }
            } else {
                print("Scrape Complete")
            }
        }
    }
    
    func scrapeComplete(parserResult: ParserResult?, link: String) {
        visitedLinks.append(link)
        if let webRecipe = parserResult?.webRecipe {
            successfullyScraped = successfullyScraped + 1
            outputRecipe(textGroup: webRecipe.ingredients, basePath: "ingredients")
            outputRecipe(textGroup: webRecipe.method, basePath: "method")
            outputRecipe(textGroup: webRecipe.otherText, basePath: "other")
        }
        if let links = parserResult?.links {
            let processedLinks = processLinks(links: links)
            for link in processedLinks {
                if !visitedLinks.contains(link) {
                    self.links.insert(link)
                }
            }
        }
        links.remove(link)
    }
    
    func deleteExisitingFiles() {
        let fileManager = FileManager.default
        let documentDirectory = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let ingredientsPath = documentDirectory.appendingPathComponent("ingredients")
        let methodPath = documentDirectory.appendingPathComponent("method")
        let other = documentDirectory.appendingPathComponent("other")
        do {
            try fileManager.removeItem(at: ingredientsPath)
            try fileManager.removeItem(at: methodPath)
            try fileManager.removeItem(at: other)
        } catch { }
        
        do {
            try fileManager.createDirectory(at: ingredientsPath, withIntermediateDirectories: false, attributes: [:])
            try fileManager.createDirectory(at: methodPath, withIntermediateDirectories: false, attributes: [:])
            try fileManager.createDirectory(at: other, withIntermediateDirectories: false, attributes: [:])
        } catch {}
    }
    
    func outputRecipe(textGroup: [RecipeSection], basePath: String) {
        let documentDirectory = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        var fileCount = 0
        
        for section in textGroup {
            for line in section.body {
                do {
                    let fileURL = documentDirectory.appendingPathComponent("\(basePath)/\(successfullyScraped)-\(fileCount).txt")
                    try line.appendLineToURL(fileURL: fileURL)
                    fileCount = fileCount + 1
                }
                catch let error {
                    print("Ooops! Something went wrong: \(error)")
                }
            }
        }
        
        
    }
    
    func processLinks(links: [String]) -> [String] {
        return links.filter { (link) -> Bool in
//            return link.components(separatedBy: "/").count == 4
            return true
        }.map { (link) -> String in
//            return "http://www.bbc.co.uk\(link)"
            return link
        }
    }
    
    func scrape(url: URL, completion: @escaping (ParserResult?) -> Void) {
        html(url: url) { [weak self] (html) in
            guard let html = html,
                let recipe = self?.recipeProvider.recipe(fromHtml: html, url: url) else {
                    completion(nil)
                    return
            }
            completion(recipe)
        }
    }
    
    func html(url: URL, completion: @escaping (String?) -> Void ) {
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                completion(nil)
                return
            }
            completion(String(data: data, encoding: .utf8))
        }
        dataTask.resume()
    }
}

extension String {
    func appendLineToURL(fileURL: URL) throws {
        try (self + "\n").appendToURL(fileURL: fileURL)
    }
    
    func appendToURL(fileURL: URL) throws {
        let data = self.data(using: String.Encoding.utf8)!
        try data.append(fileURL: fileURL)
    }
}

extension Data {
    func append(fileURL: URL) throws {
        if let fileHandle = FileHandle(forWritingAtPath: fileURL.path) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.write(self)
        }
        else {
            try write(to: fileURL, options: .atomic)
        }
    }
}
