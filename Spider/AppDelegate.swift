//
//  AppDelegate.swift
//  Spider
//
//  Created by Tim Harrison on 15/09/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

